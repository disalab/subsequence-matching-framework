/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequence.datatypes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * TODO: rework
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class PhonemeConfidenceVector extends ConfidenceVector {

    public PhonemeConfidenceVector(BufferedReader reader, int dimension) throws IOException {
        super(dimension);
        this.dimension = dimension;
        String line;

        line = reader.readLine();
        line = line.substring(3).trim();


        for (int lNum = 1; lNum < dimension/10+1; lNum++) {
            // Read the line from the input
            line = reader.readLine();
            //line.
        }
    }

    @Override
    public float getDist(ConfidenceVector obj) {
        return 0f;
    }

    @Override
    public void write(BufferedWriter stream) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
