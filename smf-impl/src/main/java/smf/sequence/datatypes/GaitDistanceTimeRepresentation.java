/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequence.datatypes;

import java.io.Serializable;
import java.util.Arrays;
import smf.sequence.impl.GaitSimiMotionObject;
import smf.sequences.Sequence;

/**
 * TODO: rework
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class GaitDistanceTimeRepresentation implements Serializable {

    /** class id for serialization */
    private static final long serialVersionUID = 1L;
    /** the distance between two specific points in time */
    protected final float[] timeDistanceHeadLeftWrist;
    protected final float[] timeDistanceHeadRightHip;

    //****************** Constructors ******************//
    /**
     * Creates a distance time representation of gait.
     *
     * @param go SimiMotion gait object
     */
    public GaitDistanceTimeRepresentation(GaitSimiMotionObject go) {
        final int gaitLength = go.getSequenceLength();
        timeDistanceHeadLeftWrist = new float[gaitLength];
        timeDistanceHeadRightHip = new float[gaitLength];
        for (int i = 0; i < gaitLength; i++) {
            timeDistanceHeadLeftWrist[i] = go.getCoords(GaitSimiMotionObject.BodyPoint.HEAD)[i].getDistanceL2(go.getCoords(GaitSimiMotionObject.BodyPoint.LEFT_WRIST)[i]);
            timeDistanceHeadRightHip[i] = go.getCoords(GaitSimiMotionObject.BodyPoint.HEAD)[i].getDistanceL2(go.getCoords(GaitSimiMotionObject.BodyPoint.RIGHT_HIP)[i]);
        }
    }

    /**
     * Creates a distance time representation of gait subsequence.
     *
     * @param gr distance time representation of gait
     * @param from position from which gait is being considered
     * @param to position to which gait is being considered
     */
    private GaitDistanceTimeRepresentation(GaitDistanceTimeRepresentation gr, int from, int to) {
        timeDistanceHeadLeftWrist = Arrays.copyOfRange(gr.timeDistanceHeadLeftWrist, from, to);
        timeDistanceHeadRightHip = Arrays.copyOfRange(gr.timeDistanceHeadRightHip, from, to);
    }

    //****************** Methods ******************//
    /**
     * Extracts a subsequence of distance-time gait representation.
     *
     * @param from position from which the subsequence is being considered
     * @param to position to which the subsequence is being considered
     * @return subsequence of distance-time gait representation
     */
    public GaitDistanceTimeRepresentation getSubsequenceData(int from, int to) {
        return new GaitDistanceTimeRepresentation(this, from, to);
    }

    /**
     * Computes the distance between two pieces of this sequence and a given sequence.
     * 
     * @param thisPiece offset of a piece in this sequence
     * @param s sequence to be compared to
     * @param sPiece offset of a piece in the sequence to be compared to
     * @return distance between two pieces of this sequence and the given sequence
     */
    public float getPiecewiseDist(int thisPiece, Sequence<GaitDistanceTimeRepresentation> s, int sPiece) {
        //return Math.abs(timeDistanceHeadLeftWrist[thisPiece] - s.getSequenceData().timeDistanceHeadLeftWrist[sPiece]);
        return Math.abs(timeDistanceHeadRightHip[thisPiece] - s.getSequenceData().timeDistanceHeadRightHip[sPiece]);
    }

    /**
     * Accesses object data in a form of sequence of numbers or several sequences of numbers.
     * 
     * @return object data in a form of sequence of numbers or several sequences of numbers
     */
    public float[][] getDataForRendering() {
        float[][] rtv = new float[2][timeDistanceHeadLeftWrist.length];
        rtv[0] = timeDistanceHeadLeftWrist;
        rtv[1] = timeDistanceHeadRightHip;
        return rtv;
    }
}
