/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequence.impl;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UnknownFormatConversionException;
import messif.objects.LocalAbstractObject;
import smf.sequences.RenderableSequence;
import smf.sequences.Sequence;

/**
 * TODO: rework
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public abstract class GaitSimiMotionObject<T> extends LocalAbstractObject implements Sequence<T>, RenderableSequence {

    //****************** Constants ******************//
    /** individual body points */
    public enum BodyPoint {

        HEAD, LEFT_SHOULDER, RIGHT_SHOULDER, LEFT_ELBOW, RIGHT_ELBOW, LEFT_WRIST, RIGHT_WRIST, LEFT_HIP, RIGHT_HIP, LEFT_KNEE, RIGHT_KNEE, LEFT_ANKLE, RIGHT_ANKLE;

        /**
         * Returns the index of a specific body point.
         * 
         * @param bodyPoint specific body point
         * @return the index of a specific body point
         */
        public int getIndex(BodyPoint bodyPoint) {
            switch (bodyPoint) {
                case HEAD:
                    return 0;
                case LEFT_SHOULDER:
                    return 1;
                case RIGHT_SHOULDER:
                    return 2;
                case LEFT_ELBOW:
                    return 3;
                case RIGHT_ELBOW:
                    return 4;
                case LEFT_WRIST:
                    return 5;
                case RIGHT_WRIST:
                    return 6;
                case LEFT_HIP:
                    return 7;
                case RIGHT_HIP:
                    return 8;
                case LEFT_KNEE:
                    return 9;
                case RIGHT_KNEE:
                    return 10;
                case LEFT_ANKLE:
                    return 11;
                case RIGHT_ANKLE:
                    return 12;
            }
            return -1;
        }
    };
    /** names of individual body points */
    protected static final String[] bodyPoints = {"head", "left shoulder", "right shoulder", "left elbow", "right elbow", "left wrist", "right wrist", "left hip", "right hip", "left knee", "right knee", "left ankle-bone", "right ankle-bone"};
    /** names of features within each body point (3d coordinate, length, velocity, and acceleration) */
    protected static final String[] bodyPointFeatures = {"X", "Y", "Z", "Length", "v(X)", "v(Y)", "v(Z)", "v(abs)", "a(X)", "a(Y)", "a(Z)", "a(abs)"};
    //****************** Data ******************//
    /** spatial coordinates of all body points in time (the time difference
     * between two neighboring coordinates is 10ms) */
    private Coordinate3d[][] bodyPointCoords;
    /** the number of times all body points have been successfully identified,
     * i.e., the sequence length */
    private final int bodyPointSequenceLength;

    
    // ******************   Sequence implementation   ******************* //

    /** Locator of the original sequence, if this is a subsequence */
    protected final String originalSequenceLocator;

    /** Offset in the {@link #originalSequence} that the {@link #sequenceData} comes from */
    private int originalOffset;

    /** The {@link #originalSequence} that the {@link #sequenceData} comes from - can be null */
    private final GaitSimiMotionObject<T> originalSequence;
    
    @Override
    public int getOffset() {
        return originalOffset;
    }

    @Override
    public String getOriginalSequenceLocator() {        
        return (originalSequenceLocator!=null)?originalSequenceLocator:this.getLocatorURI();
    }    

    @Override
    public Sequence<? extends T> getOriginalSequence() {
        return originalSequence;
    }
    
    
    
    //****************** Constructors ******************//
    /**
     * Reads the gait object from a buffered stream created by SimiMotion software.
     *
     * @param stream input stream created by SimiMotion software and processed by the extraction method
     * @throws Exception
     */
    public GaitSimiMotionObject(BufferedReader stream) throws EOFException, IOException {

        List<String[]> lines = new ArrayList<String[]>();

        // Reads the abstract object key
        String line = readObjectComments(stream);
        if (line == null) {
            throw new NoSuchElementException();
        }

        // Number of attributes
        final int attributeCount = 1 + (bodyPoints.length * bodyPointFeatures.length);
        int[][] bodyFeaturePointIndexes = new int[bodyPoints.length][bodyPointFeatures.length];
        for (int i = 0; i < bodyPoints.length; i++) {
            Arrays.fill(bodyFeaturePointIndexes[i], -1);
        }

        // Checks the header line
        String[] header = line.trim().split("\t");
        if (header.length != attributeCount) {
            reachObjectSupplement(stream, getLocatorURI() + ": the header does not contain the requested number of " + attributeCount + " attributes.");
        }
        if (!header[0].equals("Time")) {
            reachObjectSupplement(stream, getLocatorURI() + ": the header does not contain the \"Time\" attribute.");
        }
        for (int i = 0; i < bodyPoints.length; i++) {
            for (int j = 0; j < bodyPointFeatures.length; j++) {
                bodyFeaturePointIndexes[i][j] = Arrays.asList(header).indexOf(bodyPoints[i] + " " + bodyPointFeatures[j]);
                if (bodyFeaturePointIndexes[i][j] == -1) {
                    reachObjectSupplement(stream, getLocatorURI() + ": the header does not contain the \"" + bodyPoints[i] + " " + bodyPointFeatures[j] + "\" attribute.");
                }
            }
        }
        line = stream.readLine();

        // Reads coordinates until the final line is found
        while (line != null && line.indexOf("#end") == -1) {

            // Coordinates of individual points are delimited by tabular character
            String[] coords = line.trim().split("\t");

            // Coordinates along with velocity and acceleration of all points
            // must be present. Otherwise, the line is not considered.
            if (coords != null && coords.length == attributeCount) {
                boolean validLine = false;
                int i = 0;
                while (i < attributeCount) {
                    if (coords[i].trim().isEmpty()) {
                        break;
                    }
                    if (!coords[i].trim().equals(coords[0].trim())) {
                        validLine = true;
                    }
                    i++;
                }
                if (i == attributeCount && validLine) {
                    lines.add(coords);
                }
            }
            line = stream.readLine();
        }

        // The number of times all body points have been successfully identified
        bodyPointSequenceLength = lines.size();
        if (bodyPointSequenceLength == 0) {
            throw new UnknownFormatConversionException(getLocatorURI() + ": the gait representation has not been found.");
        }

        // Saves all recognized coordinates
        bodyPointCoords = new Coordinate3d[bodyPoints.length][bodyPointSequenceLength];
        for (int j = 0; j < bodyPointSequenceLength; j++) {
            String[] coords = lines.get(j);
            for (int i = 0; i < bodyPoints.length; i++) {
                bodyPointCoords[i][j] = new Coordinate3d(
                        parseFloat(coords[bodyFeaturePointIndexes[i][0]]),
                        parseFloat(coords[bodyFeaturePointIndexes[i][1]]),
                        parseFloat(coords[bodyFeaturePointIndexes[i][2]]));
            }
        }
        
        // sequence attributes
        this.originalSequenceLocator = null;
        this.originalSequence = null;
        this.originalOffset = -1;        
    }

    //****************** Methods ******************//
    /**
     * Reads lines until the end of object is reached.
     *
     * @param stream input stream from which the object is being read
     * @throws EOFException
     * @throws IOException
     */
    private void reachObjectSupplement(BufferedReader stream) throws EOFException, IOException {
        String line = stream.readLine();
        while (line != null && line.indexOf("#end") == -1) {
            line = stream.readLine();
        }
    }

    /**
     * Reads lines until the end of object is reached and throws a given
     * {@link UnknownFormatConversionException}.
     *
     * @param stream input stream from which the object is being read
     * @param String exception to be thrown
     * @throws EOFException
     * @throws IOException
     */
    private void reachObjectSupplement(BufferedReader stream, String exception) throws EOFException, IOException {
        reachObjectSupplement(stream);
        throw new UnknownFormatConversionException(exception);
    }

    /**
     * Parses a float value from a given string.
     *
     * @param value string to be parsed
     * @return parsed float value
     */
    protected final float parseFloat(String value) {
        return Float.parseFloat(value.replace(",", "."));
    }

    //****************** Body point methods ******************//
    /**
     * Returns coordinates of a specific body point moving in time.
     * 
     * @param bodyPoint specific body point
     * @return coordinates of a specific body point moving in time
     */
    public Coordinate3d[] getCoords(BodyPoint bodyPoint) {
        return bodyPointCoords[bodyPoint.getIndex(bodyPoint)];
    }
    //****************** Overrided class LocalAbstractObject ******************//

    @Override
    public int getSize() {
        return 13 * getSequenceLength() * Float.SIZE / 8;
    }

    @Override
    public boolean dataEquals(Object obj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int dataHashCode() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    //****************** Implemented interface Sequence ******************//
    /**
     * Returns the length of gait sequence.
     *
     * @return the length of gait sequence
     */
    @Override
    public int getSequenceLength() {
        return bodyPointSequenceLength;
    }

    //****************** Classes ******************//
    /**
     * The 3d coordinate that is being compared with the Euclidean distance.
     */
    public static class Coordinate3d implements Serializable {

        /** class id for serialization */
        private static final long serialVersionUID = 1L;
        /** x-axis coordinate */
        protected final float x;
        /** y-axis coordinate */
        protected final float y;
        /** z-axis coordinate */
        protected final float z;

        //****************** Constructors ******************//
        /**
         * Creates a 3d coordinate.
         *
         * @param x x-axis coordinate
         * @param y y-axis coordinate
         * @param z z-axis coordinate
         */
        public Coordinate3d(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        //****************** Methods ******************//
        /**
         * Computes the Euclidean distance between two 3d coordinates.
         *
         * @param c 3d coordinate to be compared with
         * @return the Euclidean distance between this object and and the given 3d coordinate
         */
        public float getDistanceL2(Coordinate3d c) {
            return (float) Math.sqrt((x - c.x) * (x - c.x) + (y - c.y) * (y - c.y) + (z - c.z) * (z - c.z));
        }
    }
}
