/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequence.impl;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import messif.objects.DistanceFunction;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinarySerializator;
import smf.modules.distance.L2SequenceDist;
import smf.sequences.Sequence;

/**
 * This class encapsulates an object of float sequence with L2 distance.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class SequenceFloatL2 extends SequenceFloatObject {
    
    /** Serialization class ID */
    private static final long serialVersionUID = 96101L;

    /** Creator of L2 distance function */
    @SuppressWarnings("unchecked")
    private static DistanceFunction<Sequence<float[]>> createDistanceFunction() {
        return (DistanceFunction<Sequence<float[]>>) new L2SequenceDist<float[]>();
    }
    
    
    public SequenceFloatL2(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator, createDistanceFunction());
    }

    public SequenceFloatL2(float[] data, SequenceFloatL2 originalObject, int offset, boolean storeOrigSeq) {
        super(data, offset, originalObject, createDistanceFunction(), storeOrigSeq);
    }

    public SequenceFloatL2(float[] data) {
        super(data, createDistanceFunction());
    }

    public SequenceFloatL2(BufferedReader stream) throws EOFException, IOException, NumberFormatException {
        super(stream, createDistanceFunction());
    }
    
    /**
     * Deserialize and set the transient fields.
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        distFunction = createDistanceFunction();        
    }

    /**
     * Returns a string containing this sequence description
     * @return description of this object (object class, key, sequence length, parent key, offset)
     */
    @Override
    public String toString() {
        return "Euclidean " + super.toString();
   }
}
