/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequence.impl;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import messif.objects.DistanceFunction;
import messif.objects.LocalAbstractObject;
import messif.objects.impl.ObjectFloatVector;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializator;
import smf.sequences.Sequence;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public abstract class SequenceFloatObject extends ObjectFloatVector implements Sequence<float[]> {
    
    /** Class serial version ID for serialization. */
    private static final long serialVersionUID = 78101L;

    /** Locator of the original sequence, if this is a subsequence */
    protected final String originalSequenceLocator;
    /**
     * Offset in the {@link #originalSequence} that the {@link #sequenceData}
     * comes from
     */
    private final int originalOffset;
    /**
     * The {@link #originalSequence} that the {@link #sequenceData} comes from -
     * can be null
     */
    private final SequenceFloatObject originalSequence;
    
    /** Distance function to be used for pairs of object of this type */
    protected transient DistanceFunction<Sequence<float[]>> distFunction;
    
    public SequenceFloatObject(BufferedReader stream, DistanceFunction<Sequence<float[]>> distFunction) throws EOFException, IOException, NumberFormatException {
        super(stream);        
        this.distFunction = distFunction;
        this.originalSequenceLocator = null;
        this.originalSequence = null;
        this.originalOffset = -1;
    }

    public SequenceFloatObject(float[] data, DistanceFunction<Sequence<float[]>> distFunction) {
        super(data);
        this.distFunction = distFunction;
        this.originalSequenceLocator = null;
        this.originalSequence = null;
        this.originalOffset = -1;
    }

    public SequenceFloatObject(float[] data, int offset, SequenceFloatObject originalObject, DistanceFunction<Sequence<float[]>> distFunction, boolean storeOrigSeq) {
        super(data);
        this.distFunction = distFunction;
        this.originalOffset = offset;
        this.originalSequenceLocator = originalObject.getLocatorURI();
        this.originalSequence = storeOrigSeq ? originalObject : null;
    }

    @Override
    public float[] getSequenceData() {
        return getVectorData();
    }

    @Override
    public float[] getSubsequenceData(int from, int to) {
        return Arrays.copyOfRange(data, from, to);
    }

    @Override
    public int getSequenceLength() {
        return getDimensionality();
    }

    @Override
    public Class<? extends float[]> getSequenceDataClass() {
        return float[].class;
    }

    @Override
    public int getOffset() {
        return originalOffset;
    }

    @Override
    public String getOriginalSequenceLocator() {        
        return (originalSequenceLocator != null) ? originalSequenceLocator : this.getLocatorURI();
    }

    @Override
    public SequenceFloatObject getOriginalSequence() {
        return originalSequence;
    }

    /**
     * Returns the distance computed by the instance of (
     *
     * @see messif.objects.DistanceFunction) stored in a distFunction attribute
     * @param obj object to calculate distance from
     * @param distThreshold (not used, actually)
     * @return distance between this object and the given object
     */
    @Override
    protected float getDistanceImpl(LocalAbstractObject obj, float distThreshold) {
        // Compute distance
        if (!(obj instanceof SequenceFloatObject)) {
            return LocalAbstractObject.UNKNOWN_DISTANCE;
        }
        return distFunction.getDistance(this, (SequenceFloatObject) obj);
    }
    
    @Override
    public float getPiecewiseDist(int thisPiece, Sequence<float[]> s, int sPiece) {
        if (s instanceof SequenceFloatObject) {
            return Math.abs(data[thisPiece] - ((SequenceFloatObject) s).data[sPiece]);
        }
        return Math.abs(data[thisPiece] - s.getSequenceData()[sPiece]);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Float sequence  (").append(getObjectKey()).append(") lentgth=").append(getSequenceLength()).append(", offset=").
                append(getOffset()).append(", parent=").append(getOriginalSequenceLocator()).append(": ").append(super.toString());
        return stringBuilder.toString();
    }
 
    //************ BinarySerializable interface ************//

    /**
     * Creates a new instance of ObjectFloatVector loaded from binary input
     * buffer.
     *
     * @param input the buffer to read the ObjectFloatVector from
     * @param serializator the serializator used to write objects
     * @param distFunction distance 
     * @throws IOException if there was an I/O error reading from the buffer
     */
    protected SequenceFloatObject(BinaryInput input, BinarySerializator serializator, DistanceFunction<Sequence<float[]>> distFunction) throws IOException {
        super(input, serializator);
        this.originalOffset = serializator.readInt(input);
        this.originalSequenceLocator = serializator.readString(input);
        this.originalSequence = serializator.readObject(input, SequenceFloatObject.class);
        this.distFunction = distFunction;
    }

    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        return super.binarySerialize(output, serializator) + serializator.write(output, originalOffset)
                + serializator.write(output, originalSequenceLocator) + serializator.write(output, originalSequence);
               
    }

    @Override
    public int getBinarySize(BinarySerializator serializator) {
        return super.getBinarySize(serializator) + serializator.getBinarySize(originalOffset)
                + serializator.getBinarySize(originalSequenceLocator) + serializator.getBinarySize(originalSequence);
    }
}