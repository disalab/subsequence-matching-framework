/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequence.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import messif.objects.LocalAbstractObject;
import smf.sequence.datatypes.ConfidenceVector;
import smf.sequences.DistanceAllowsNonEquilength;
import smf.sequences.Sequence;

/**
 * TODO: rework
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 * @deprecated until reworked
 */
@Deprecated
public class PosteriogramTemplateDTW extends LocalAbstractObject implements Sequence<List<ConfidenceVector>>, DistanceAllowsNonEquilength {

    private List<ConfidenceVector> data;

    //private Sequence<List<ConfidenceVector>> originalData;

    /** Locator of the original sequence, if this is a subsequence */
    protected final String originalSequenceLocator;

    /** Offset in the {@link #originalSequence} that the {@link #sequenceData} comes from */
    private final int originalOffset;

    /** The {@link #originalSequence} that the {@link #sequenceData} comes from - can be null */
    private final PosteriogramTemplateDTW originalSequence;

    public PosteriogramTemplateDTW(BufferedReader stream) {
        data = new LinkedList<ConfidenceVector>();

        this.originalSequenceLocator = null;
        this.originalSequence = null;
        this.originalOffset = -1;
    }

    @Override
    protected float getDistanceImpl(LocalAbstractObject obj, float distThreshold) {
        // TODO Modified DTW
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<ConfidenceVector> getSequenceData() {
        return data;
    }

    public List<ConfidenceVector> getSubsequenceData(int from, int to) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSize() {
        // LocalAbstractObject size
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean dataEquals(Object obj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int dataHashCode() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        // TODO write data posteriogram in a form of phnrec output
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getSequenceLength() {
        return data.size();
    }

    @Override
    public Class getSequenceDataClass() {
        return List.class;
    }

    @Override
    public int getOffset() {
        return originalOffset;
    }

    @Override
    public String getOriginalSequenceLocator() {
        return originalSequenceLocator;
    }

    @Override
    public Sequence<? extends List<ConfidenceVector>> getOriginalSequence() {
        return originalSequence;
    }
    
    @Override
    public float getPiecewiseDist(int thisPiece, Sequence<List<ConfidenceVector>> s, int sPiece) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
