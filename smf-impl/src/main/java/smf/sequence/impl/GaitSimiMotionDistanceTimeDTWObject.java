/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequence.impl;

import smf.sequence.datatypes.GaitDistanceTimeRepresentation;
import java.io.BufferedReader;
import messif.objects.DistanceFunction;
import messif.objects.LocalAbstractObject;
import smf.modules.distance.DTWSequenceDist;
import smf.sequences.DistanceAllowsNonEquilength;
import smf.sequences.Sequence;

/**
 * TODO: rework (probably shift distFUnction to predecessor; create slicing constructors; 
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class GaitSimiMotionDistanceTimeDTWObject extends GaitSimiMotionObject<GaitDistanceTimeRepresentation> implements DistanceAllowsNonEquilength {

    /** class id for serialization */
    private static final long serialVersionUID = 1L;
    /** gait representation */
    protected final GaitDistanceTimeRepresentation gr;
    /** distance function */
    private final DistanceFunction distFunction = new DTWSequenceDist();

    //****************** Constructors ******************//
    /**
     * Creates the gait distance time object from a buffered stream created by SimiMotion software.
     *
     * @param stream input stream created by SimiMotion software and processed by the extraction method
     * @throws Exception
     */
    public GaitSimiMotionDistanceTimeDTWObject(BufferedReader stream) throws Exception {
        super(stream);
        gr = new GaitDistanceTimeRepresentation(this);
    }

    //****************** Overrided class LocalAbstractObject ******************//
    /**
     * Computes the DTW distance between two distance-time gait representations.
     *
     * @param obj gait representation to be compared with
     * @param distThreshold
     * @return the DTW distance between this object and the given distance-time gait representation
     */
    @Override
    protected float getDistanceImpl(LocalAbstractObject obj, float distThreshold) {
        // Computes DTW
        return distFunction.getDistance(this, obj);
    }

    //****************** Implemented interface Sequence ******************//
    @Override
    public GaitDistanceTimeRepresentation getSequenceData() {
        return gr;
    }

    @Override
    public Class<? extends GaitDistanceTimeRepresentation> getSequenceDataClass() {
        return GaitDistanceTimeRepresentation.class;
    }

    @Override
    public GaitDistanceTimeRepresentation getSubsequenceData(int from, int to) {
        return gr.getSubsequenceData(from, to);
    }


    @Override
    public float getPiecewiseDist(int thisPiece, Sequence<GaitDistanceTimeRepresentation> s, int sPiece) {
        return gr.getPiecewiseDist(thisPiece, s, sPiece);
    }

    //****************** Implemented interface RenderableSequence ******************//
    /**
     * Accesses object data in a form of sequence of numbers or several sequences of numbers.
     * 
     * @return object data in a form of sequence of numbers or several sequences of numbers
     */
    @Override
    public float[][] getDataForRendering() {
        return gr.getDataForRendering();
    }
}
