/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.transformer;

import smf.modules.SequenceTransformer;
import smf.sequences.Sequence;

/**
 * This class represents Piecewise Aggregate Approximation dimensionality reduction
 * technique over the float based sequences.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class PAATransformerFloatArray implements SequenceTransformer<float[]> {

    /** The number of values that should be aggregated into one approximate value */
    private final int pieceLength;

    /**
     * Constructor with given length of PAA piece
     * @param pieceLength number of values that will be reduced into one average value
     */
    public PAATransformerFloatArray(int pieceLength) {
        this.pieceLength = pieceLength;
    }

    /**
     * Transforms the original sequence into the PAA representation parametrized by the pieceLnegth attribute.
     * The process of PAA takes every pieceLength values and makes an average value from them which is a reduced
     * representation of the original vaules.
     * @param sequence
     * @return
     */
    @Override
    public float[] transform(Sequence<? extends float[]> sequence) {
        float[] orig = sequence.getSequenceData();
        int retLength = (sequence.getSequenceLength() % pieceLength) == 0 ? sequence.getSequenceLength() / pieceLength : sequence.getSequenceLength() / pieceLength + 1;
        float[] ret = new float[retLength];
        float sum = 0;
        int j;

        // Compute average values
        for (int i = 0; i < sequence.getSequenceLength() / pieceLength; i++) {
            for (j = i * pieceLength; j < i * pieceLength + pieceLength; j++) {
                sum += orig[j];
            }
            ret[i] = sum / pieceLength;
            sum = 0;
        }
        // Last piece can be shorter
        if (sequence.getSequenceLength() % pieceLength != 0) {
            j = 0;
            for (int i = (ret.length - 1) * pieceLength; i < orig.length; i++) {
                sum+=orig[i];
                j++;
            }
            ret[ret.length - 1] = sum/(float)j;
        }
        return ret;
    }
}
