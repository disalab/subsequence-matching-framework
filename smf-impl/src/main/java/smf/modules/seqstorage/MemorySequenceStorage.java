/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.seqstorage;

import java.lang.reflect.InvocationTargetException;
import messif.buckets.BucketStorageException;
import messif.buckets.LocalBucket;
import messif.buckets.impl.MemoryStorageLocatorBucket;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.QueryOperation;
import messif.utility.reflection.NoSuchInstantiatorException;
import smf.exceptions.StorageException;
import smf.modules.SequenceStorage;
import smf.modules.slicer.SingleSubsequenceSlicer;
import smf.sequences.Sequence;
import smf.sequences.SequenceFactory;

/**
 * Simple implementation of whole Sequence storage
 * 
 * @param <T> type of internal sequence data (e.g. float[])
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class MemorySequenceStorage<T, O extends Sequence<T>> implements SequenceStorage<T, O> {

    /** Storage with all the sequences */
    private final LocalBucket wholeStorage;
    
    /** Internal single subsequence slicer */
    private SingleSubsequenceSlicer<T> slicer = new SingleSubsequenceSlicer<T>();
    
    public MemorySequenceStorage() {
        wholeStorage = new MemoryStorageLocatorBucket(Long.MAX_VALUE, Long.MAX_VALUE, 0l, false);
    }

    @Override
    public O getSubsequence(String locator, int from, int to) throws StorageException {
        return getSubsequence(locator, from, to, true);
    }

    @Override
    public synchronized O getSubsequence(String locator, int from, int to, boolean storeParentSequence) throws StorageException {
        try {
            O sequence = getSequence(locator);
            slicer.setFromTo(Math.max(from,0), Math.min(to, sequence.getSequenceLength()));
            return SequenceFactory.create(sequence, slicer, storeParentSequence).get(0);
        } catch (NoSuchInstantiatorException ex) {
            throw new StorageException(ex);
        } catch (InvocationTargetException ex) {
            throw new StorageException(ex);
        }
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public O getSequence(String locator) {
        return (O) wholeStorage.getObject(locator);
    }

    @Override
    public void insertSequence(O sequence) throws StorageException {
        try {
            wholeStorage.addObject((LocalAbstractObject) sequence);
        } catch (BucketStorageException ex) {
            throw new StorageException(ex);
        }
    }
    
    @Override
    public int processQuery(QueryOperation<?> query) {
        return wholeStorage.processQuery(query);
    }

    @Override
    public int getObjectCount() {
        return wholeStorage.getObjectCount();
    }

    @Override
    public AbstractObjectIterator<LocalAbstractObject> getAllObjects() {
        return wholeStorage.getAllObjects();
    }

    @Override
    public void destroy() throws Throwable {
        wholeStorage.destroy();
    }

    @Override
    public void finalize() throws Throwable {
        wholeStorage.finalize();
    }
}
