/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.index;

import messif.buckets.impl.MemoryStorageBucket;
import messif.objects.LocalAbstractObject;

/**
 * Encapsulation of a memory sequential scan as a subsequence index
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class MemorySeqScanDistanceIndex<O extends LocalAbstractObject> extends BucketDistanceIndex<O> {
    
    /**
     * Given an already created algorithm instance, this constructor creates an
     *  AlgorithmStorageBucket and uses it for creating BucketDistanceIndex.
     * @param algorithm MESSIF algorithm (typically index)
     */
    public MemorySeqScanDistanceIndex() {
        super(new MemoryStorageBucket(Long.MAX_VALUE, Long.MAX_VALUE, 0l, false));
    }
        
}
