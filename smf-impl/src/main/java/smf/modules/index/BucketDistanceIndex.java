/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.index;

import java.util.Collection;
import java.util.Iterator;
import messif.buckets.BucketStorageException;
import messif.buckets.LocalBucket;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.operations.AnswerType;
import messif.operations.QueryOperation;
import messif.operations.query.KNNQueryOperation;
import smf.exceptions.StorageException;
import smf.modules.DistanceIndex;

/**
 * Encapsulation of any bucket as a SMF distance index
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class BucketDistanceIndex<O extends LocalAbstractObject> extends DistanceIndex<O> {

    /** Storage with sequence objects */
    private final LocalBucket storage;

    /** Creator given an already created bucket */
    public BucketDistanceIndex(LocalBucket storage) {
        this.storage = storage;
    }    

    /**
     * A default implementation of creator of operation that should be executed on the encapsulated 
     *   bucket when nearest objects required.
     * @param object query object
     * @param k number of nearest neighbors wanted
     * @return operation that should be executed on the encapsulated bucket 
     */
    protected KNNQueryOperation createQueryOperation(O object, int k) {
        return new KNNQueryOperation(object, k, AnswerType.ORIGINAL_OBJECTS);
    }
    
    @Override
    public void addObject(O object) throws StorageException {
        try {
            storage.addObject(object);
        } catch (BucketStorageException ex) {
            throw new StorageException(ex);
        }
    }

    @Override
    public void addObjects(Collection<O> objects) throws StorageException {
        try {
            storage.addObjects(objects);
        } catch (BucketStorageException ex) {
            throw new StorageException(ex);
        }
    }
    
    @Override
    public Iterator<RankedAbstractObject> findNearest(O object, int k) throws StorageException {
        KNNQueryOperation op = createQueryOperation(object, k);
        processQuery(op);
        return op.getAnswer();
    }

    @Override
    public int processQuery(QueryOperation<?> query) {
        return storage.processQuery(query);
    }

    @Override
    public int getObjectCount() {
        return storage.getObjectCount();
    }

    @Override
    public void finalize() throws Throwable {
        storage.finalize();
    }

    @Override
    public void destroy() throws Throwable {
        storage.destroy();
    }
    
}
