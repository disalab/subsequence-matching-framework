/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.index;

import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.impl.AlgorithmStorageBucket;
import messif.objects.LocalAbstractObject;
import messif.operations.AnswerType;
import messif.operations.Approximate;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.operations.query.KNNQueryOperation;

/**
 * Encapsulation of any bucket as a SMF distance index
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class ApproximateAlgorithmDistanceIndex<O extends LocalAbstractObject> extends BucketDistanceIndex<O> {

    /** The approximate type can be set and is then used for all approx. kNN operations */
    private Approximate.LocalSearchType approxSearchType = Approximate.LocalSearchType.USE_STRUCTURE_DEFAULT;
    
    /** Approximate precision (used when the {@link #approxSearchType} is not {@code Approximate.LocalSearchType.USE_STRUCTURE_DEFAULT} */
    private int approxPrecition = 0;
    
    /**
     * Given an already created algorithm instance, this constructor creates an
     *  AlgorithmStorageBucket and uses it for creating BucketDistanceIndex.
     *  The default approx. precision is used within the algorithm to resolve Appox. kNN.
     * @param algorithm MESSIF algorithm (typically index)
     */
    public ApproximateAlgorithmDistanceIndex(Algorithm algorithm) throws AlgorithmMethodException {
        super(new AlgorithmStorageBucket(algorithm, Long.MAX_VALUE, Long.MAX_VALUE, 0l, false));
    }

    /**
     * Given an already created algorithm instance, this constructor creates an
     *  AlgorithmStorageBucket and uses it for creating BucketDistanceIndex.
     * @param algorithm MESSIF algorithm (typically index)
     * @param approxSearchType type of the approx. precision limitation
     * @param approxPrecition value of the approx. precision limitation
     */
    public ApproximateAlgorithmDistanceIndex(Algorithm algorithm, Approximate.LocalSearchType approxSearchType, int approxPrecition) throws AlgorithmMethodException {
        this(algorithm);
        this.approxSearchType = approxSearchType;
        this.approxPrecition = approxPrecition;
    }
    
    /**
     * Creator of kNN approximate operation with default approximate precision.
     * @param object query object
     * @param k number of nearest neighbors wanted
     * @return operation that should be executed on the encapsulated bucket 
     */
    @Override
    protected KNNQueryOperation createQueryOperation(O object, int k) {
        return new ApproxKNNQueryOperation(object, k, AnswerType.ORIGINAL_OBJECTS, approxPrecition, approxSearchType, -1f);
    }
    
}
