/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.distance;

import messif.objects.DistanceFunction;
import smf.sequences.DistanceAllowsNonEquilength;
import smf.sequences.Sequence;

/**
 * This method encapsulates an implementation of the Move-Split-Merge(MSM)
 * distance for float sequences.
 *
 * @author Jakub Valcik, Masaryk University, Brno, Czech Republic,
 * xvalcik@fi.muni.cz
 */
public class MSMFloatSequenceDist implements DistanceFunction<Sequence<float[]>>, DistanceAllowsNonEquilength {

    private static final float SPLIT_MERGE_COST = 1;

    /**
     * Computes MSM distance between two sequences in O(n*m)
     *
     * @param s1 first Sequence object
     * @param s2 second Sequence object
     * @return MSM distance between the first and the second sequence object. If
     * at least one of the given objects is not of a type Sequence than -1 is
     * returned
     */
    @Override
    public float getDistance(Sequence<float[]> s1, Sequence<float[]> s2) {
        return msm(s1.getSequenceData(), s2.getSequenceData());
    }

    @Override
    public Class<? extends Sequence<float[]>> getDistanceObjectClass() {
        return (Class) Sequence.class;
    }

    private float msm(float[] seq1, float[] seq2) {
        float[][] cost = new float[seq1.length][seq2.length];
        cost[0][0] = Math.abs(seq1[0] - seq2[0]);

        for (int i = 1; i < seq1.length; i++) {
            cost[i][0] = cost[i - 1][0] + splitMergeCost(seq1[i], seq1[i - 1], seq2[0]);
        }

        for (int j = 1; j < seq2.length; j++) {
            cost[0][j] = cost[0][j - 1] + splitMergeCost(seq2[j], seq1[0], seq2[j - 1]);
        }

        for (int i = 1; i < seq1.length; i++) {
            for (int j = 1; j < seq2.length; j++) {
                cost[i][j] = Math.min(cost[i - 1][j - 1] + Math.abs(seq1[i] - seq2[j]),
                        Math.min(cost[i - 1][j] + splitMergeCost(seq1[i], seq1[i - 1], seq2[j]),
                        cost[i][j - 1] + splitMergeCost(seq2[j], seq1[i], seq2[j - 1])));
            }
        }
        return cost[seq1.length - 1][seq2.length - 1];
    }

    private float splitMergeCost(float f1, float f2, float f3) {
        if (f2 <= f1 && f1 <= f3 || f2 > f1 && f1 > f3) {
            return SPLIT_MERGE_COST;
        } else {
            return SPLIT_MERGE_COST + Math.min(Math.abs(f1 - f2), Math.abs(f1 - f3));
        }
    }
}
