package smf.modules.distance;

import messif.objects.DistanceFunction;
import smf.sequences.DistanceAllowsNonEquilength;
import smf.sequences.Sequence;

/**
 * The normalized uniform time warping distance function.
 *
 * @param <T> type of the sequence data, usually a static array of a primitive
 * type or {@link java.util.List}
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class UTWSequenceDistNormalized<T> implements DistanceFunction<Sequence<T>>, DistanceAllowsNonEquilength {

    //************ Implemented interface DistanceFunction ************//
    @Override
    public float getDistance(Sequence<T> o1, Sequence<T> o2) {
        Sequence<T> shorterObj = o1;
        Sequence<T> longerObj = o2;
        if (shorterObj.getSequenceLength() > longerObj.getSequenceLength()) {
            shorterObj = o2;
            longerObj = o1;
        }
        float lengthRatio = (float) shorterObj.getSequenceLength() / longerObj.getSequenceLength();
        float dist = 0f;
        for (int i = 0; i < longerObj.getSequenceLength(); i++) {
            dist += longerObj.getPiecewiseDist(i, shorterObj, (int) Math.floor(i * lengthRatio));
        }
        return dist / longerObj.getSequenceLength();
    }

    @Override
    public Class<? extends Sequence<T>> getDistanceObjectClass() {
        return (Class) Sequence.class;
    }
}
