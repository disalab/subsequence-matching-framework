/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.distance;

import messif.objects.DistanceFunction;
import smf.sequences.DistanceAllowsNonEquilength;
import smf.sequences.Sequence;

/**
 * This method encapsulates an implementation of the DTW distance for sequences.
 *
 * @param <T> type of the sequence data
 * @author David Novak, Masaryk University, Brno, Czech Republic,
 * david.novak@fi.muni.cz*
 * @author Petr Volny, Masaryk University, Brno, Czech Republic,
 * volny.petr@gmail.com
 * @author Jakub Valcik, Masaryk University, Brno, Czech Republic,
 * xvalcik@fi.muni.cz
 */
public class DTWSequenceDist<T> implements DistanceFunction<Sequence<T>>, DistanceAllowsNonEquilength {

    /**
     * Computes DTW distance between two sequences
     *
     * @param s1 first Sequence object
     * @param s2 second Sequence object
     * @return DTW distance between the first and the second sequence object. If
     * at least one of the given objects is not of a type Sequence than -1 is
     * returned
     */
    @Override
    public float getDistance(Sequence<T> s1, Sequence<T> s2) {
        int n = s1.getSequenceLength(), m = s2.getSequenceLength();
        float cm[][] = new float[n][m]; // accum matrix
        // create the accum matrix
        cm[0][0] = s1.getPiecewiseDist(0, s2, 0);
        for (int i = 1; i < n; i++) {
            cm[i][0] = s1.getPiecewiseDist(i, s2, 0) + cm[i - 1][0];
        }
        for (int j = 1; j < m; j++) {
            cm[0][j] = s1.getPiecewiseDist(0, s2, j) + cm[0][j - 1];
        }
        // Compute the matrix values
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                // Decide on the path with minimum distance so far
                cm[i][j] = s1.getPiecewiseDist(i, s2, j) + Math.min(cm[i - 1][j], Math.min(cm[i][j - 1], cm[i - 1][j - 1]));
            }
        }
        return cm[n - 1][m - 1];
    }

    @Override
    public Class<? extends Sequence<T>> getDistanceObjectClass() {
        return (Class) Sequence.class;
    }
}
