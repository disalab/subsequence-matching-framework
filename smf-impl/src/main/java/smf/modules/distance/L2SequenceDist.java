/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.distance;

import messif.objects.DistanceFunction;
import smf.sequences.Sequence;

/**
 * Implementation of Euclidean distance (L2) on sequences. It returns the
 * distances for sequences of the same length and a negative number otherwise
 * (difference of the lengths).
 *
 * @param <T> sequence data type
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class L2SequenceDist<T> implements DistanceFunction<Sequence<T>> {

    @Override
    public float getDistance(Sequence<T> s1, Sequence<T> s2) {
        if (s1.getSequenceLength() != s2.getSequenceLength()) {
            return -Math.abs(s1.getSequenceLength() - s2.getSequenceLength());
        }
        float res = 0f;
        for (int i = 0; i < s1.getSequenceLength(); i++) {
            float componentDist = s1.getPiecewiseDist(i, s2, i);
            res += componentDist * componentDist;
        }
        return (float) Math.sqrt(res);
    }

    @Override
    public Class<? extends Sequence<T>> getDistanceObjectClass() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
