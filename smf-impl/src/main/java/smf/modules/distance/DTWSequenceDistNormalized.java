package smf.modules.distance;

import messif.objects.DistanceFunction;
import smf.sequences.DistanceAllowsNonEquilength;
import smf.sequences.Sequence;

/**
 * This method encapsulates an implementation of the DTW distance for sequences.
 *
 * @param <T> type of the sequence data
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class DTWSequenceDistNormalized<T> implements DistanceFunction<Sequence<T>>, DistanceAllowsNonEquilength {

    /**
     * Computes DTW distance between two sequences and normalizes it according
     * to the length of warping path.
     *
     * @param s1 first Sequence object
     * @param s2 second Sequence object
     * @return DTW distance between the first and the second sequence object. If
     * at least one of the given objects is not of a type Sequence than -1 is
     * returned
     */
    @Override
    public float getDistance(Sequence<T> s1, Sequence<T> s2) {

        int n = s1.getSequenceLength(), m = s2.getSequenceLength();
        int pm[][] = new int[n][m]; // predecessor matrix
        float cm[][] = new float[n][m]; // accum matrix

        // create the accum matrix
        cm[0][0] = s1.getPiecewiseDist(0, s2, 0);
        pm[0][0] = -1;
        for (int i = 1; i < n; i++) {
            cm[i][0] = s1.getPiecewiseDist(i, s2, 0) + cm[i - 1][0];
            pm[i][0] = 2;
        }
        for (int j = 1; j < m; j++) {
            cm[0][j] = s1.getPiecewiseDist(0, s2, j) + cm[0][j - 1];
            pm[0][j] = 0;
        }
        // Compute the matrix values
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                float minValue;
                int minIndex;
                // minIndex value matrix:
                // 1 2
                // 0 x
                if (cm[i][j - 1] <= cm[i - 1][j - 1]) {
                    minIndex = 0;
                    minValue = cm[i][j - 1];
                } else {
                    minIndex = 1;
                    minValue = cm[i - 1][j - 1];
                }
                if (cm[i - 1][j] <= minValue) {
                    minIndex = 2;
                    minValue = cm[i - 1][j];
                }
                pm[i][j] = minIndex;
                cm[i][j] = s1.getPiecewiseDist(i, s2, j) + minValue;
            }
        }

        int i = n - 1;
        int j = m - 1;
        int optimalPathLength = 1;
        while (i != 0 || j != 0) {
            switch (pm[i][j]) {
                case 0:
                    j--;
                    break;
                case 1:
                    i--;
                    j--;
                    break;
                case 2:
                    i--;
                    break;
            }
            optimalPathLength++;
        }
        return cm[n - 1][m - 1] / (float) optimalPathLength;
    }

    @Override
    public Class<? extends Sequence<T>> getDistanceObjectClass() {
        return (Class) Sequence.class;
    }
}
