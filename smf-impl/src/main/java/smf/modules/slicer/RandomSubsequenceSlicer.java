/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.slicer;

import java.util.List;
import java.util.Random;
import smf.sequences.Sequence;
import smf.sequences.SequenceSlice;

/**
 * This slicer retrieves a random subsequence (of a given length) from
 *  a given sequence.
 * 
 * @param <T> type of internal sequence data (e.g. float[])
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RandomSubsequenceSlicer<T> extends SingleSubsequenceSlicer<T> {

    /** Window size */
    protected final int window;

    /** Random generator */
    private Random random = new Random();
    
    /**
     * Constructor
     * @param window size of the window
     */
    public RandomSubsequenceSlicer(int window) {
        this.window = window;
    }

    @Override
    public synchronized List<? extends SequenceSlice<T>> slice(Sequence<? extends T> sequence) {
        if (sequence.getSequenceLength() <= window) {
            setFromTo(0, sequence.getSequenceLength());
        } else {
            int localFrom = random.nextInt(sequence.getSequenceLength() - window + 1);
            setFromTo(localFrom, localFrom + window);
        }
        return super.slice(sequence);
    }
}
