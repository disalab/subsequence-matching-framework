/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.slicer;

import java.util.LinkedList;
import java.util.List;
import smf.modules.SequenceSlicer;
import smf.sequences.Sequence;
import smf.sequences.SequenceSlice;

/**
 * AllPossibleWindowsSlicer is an implementation of the {@link SequenceSlicer}.
 * It slices the given time series into all possible windows with constraints of
 * minimal and maximal length given by constructor parameters
 *
 * @param <T> type of internal sequence data (e.g. float[])
 * @author Jakub Valcik, Masaryk University, Brno, Czech Republic,
 * xvalcik@fi.muni.cz
 */
public class AllPossibleWindowsSlicer<T> implements SequenceSlicer<T> {

    /**
     * Minimal size of the window (width of resulting slices)
     */
    private final int minWidth;
    /**
     * Maximal size of the window (width of resulting slices)
     */
    private final int maxWidth;
    /**
     * Where to start cutting windows. Subsequence originalLocator hack.
     */
    private final int offset;
    /**
     * Where windows cutting ends. Subsequence originalLocator hack.
     */
    private final int length;

    /**
     * Constructs a new object of AllPossibleWindowsSlicer. When this
     * constructor is used than the minimal window width is set to 1 and maximal
     * window width is set to MAX_INT.
     */
    public AllPossibleWindowsSlicer() {
        this(0, Integer.MAX_VALUE, 1, Integer.MAX_VALUE);
    }

    /**
     * Constructs a new object of AllPossibleWindowsSlicer.
     * @param offset
     * @param length
     * @param minWidth
     * @param maxWidth 
     */
    public AllPossibleWindowsSlicer(int offset, int length, int minWidth, int maxWidth) {
        if (minWidth < 1) {
            throw new IllegalArgumentException("Minimal window width must be greater then 0.");
        }
        if (offset < 0) {
            offset = 0;
        }
        if (maxWidth < minWidth) {
            throw new IllegalArgumentException("Maximal window width must be greater or equal then minimal window width.");
        }
        if (length < 1) {
            throw new IllegalArgumentException("Length must be greater then 0.");
        }
        this.minWidth = minWidth;
        this.maxWidth = maxWidth;
        this.offset = offset;
        this.length = length;
    }

    @Override
    public List<? extends SequenceSlice<T>> slice(Sequence<? extends T> sequence) {
        List<SequenceSlice<T>> ret = new LinkedList<SequenceSlice<T>>();
        int maxLength = Math.min(length, sequence.getSequenceLength()-offset);
        for (int i = minWidth; i <= maxLength; i++) {
            for (int j = (i - maxWidth <= 0) ? 0 : i - maxWidth; j <= i - minWidth; j++) {
                ret.add(new SequenceSlice<T>(sequence.getSubsequenceData(j+offset, i+offset), sequence, j+offset));
            }
        }
        return ret;
    }
}
