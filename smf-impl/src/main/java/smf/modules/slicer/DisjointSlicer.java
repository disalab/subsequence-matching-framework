/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.slicer;

import java.util.LinkedList;
import java.util.List;
import smf.modules.SequenceSlicer;
import smf.sequences.Sequence;
import smf.sequences.SequenceSlice;

/**
 * SlidingSlicer is an implementation of the {@link SequenceSlicer}.
 * It slices the given time series into disjoint windows which means that windows
 * don't share any part of the given time series. If a tail of the time series
 * being sliced is smaller than the given size of the window, than an extra window positioned
 * (time_series_length - window_size, time_series_length) is added to the returned list of slices.
 * 
 * @param <T> type of internal sequence data (e.g. float[])
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class DisjointSlicer<T> implements SequenceSlicer<T> {

    /**
     * Size of the window (width of resulting slices)
     */
    private final int window;

    /**
     * Constructs an instance of DisjointSlicer
     * @param window window size
     */
    public DisjointSlicer(int window) {
        this.window = window;
    }

    @Override
    public List<? extends SequenceSlice<T>> slice(Sequence<? extends T> sequence) {
        List<SequenceSlice<T>> ret = new LinkedList<SequenceSlice<T>>();
        int i = 0;

        for (i = 0; i + window <= sequence.getSequenceLength(); i += window) {            
            ret.add(new SequenceSlice<T>(sequence.getSubsequenceData(i, i + window), sequence, i));
        }

        /* Ad an extra window if there is a tail smaller than the window size at the end of the time series*/
        if (sequence.getSequenceLength() % window != 0 && sequence.getSequenceLength() >= window) {
            ret.add(new SequenceSlice<T>(sequence.getSubsequenceData(
                    sequence.getSequenceLength() - window,
                    sequence.getSequenceLength()), sequence, sequence.getSequenceLength() - window));
        }
        return ret;
    }
}
