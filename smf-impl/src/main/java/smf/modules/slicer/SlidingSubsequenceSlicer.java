/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.slicer;

import java.util.LinkedList;
import java.util.List;
import smf.modules.SequenceSlicer;
import smf.sequences.Sequence;
import smf.sequences.SequenceSlice;

/**
 * SlidingSlicer is an implementation of the {@link SequenceSlicer}. It slices
 * the given time series into sliding windows which means that the returned
 * windows overlap each other in a way parameterized by the step parameter.
 *
 * @param <T> type of internal sequence data (e.g. float[])
 * @author David Novak, Masaryk University, Brno, Czech Republic,
 * david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic,
 * volny.petr@gmail.com
 */
public class SlidingSubsequenceSlicer<T> implements SequenceSlicer<T> {

    /**
     * Size of the window (width of resulting slices)
     */
    private final int window;
    /**
     * Step by which the slicer window will move, typically 1.
     */
    private final int step;
    /**
     * Where to start cutting windows. Subsequence originalLocator hack.
     */
    private final int offset;
    /**
     * Where windows cutting ends. Subsequence originalLocator hack.
     */
    private final int length;

    /**
     * Constructs a new object of SlidingSlicer. When this constructor is used
     * than the step parameter is set to 1.
     *
     * @param window window size
     */
    public SlidingSubsequenceSlicer(int window) {
        this(window, 1);
    }

    /**
     * Constructs a new object of SlidingSlicer.
     *
     * @param window window size
     * @param step size of the step between two overlapping windows
     */
    public SlidingSubsequenceSlicer(int window, int step) {
        this(window, step, 0, Integer.MAX_VALUE);
    }

    public SlidingSubsequenceSlicer(int window, int step, int offset, int length) {
        if (offset < 0) {
            offset = 0;
        }
        if (length < 1) {
            throw new IllegalArgumentException("Length must be greater then 0.");
        }
        this.window = window;
        this.step = step;
        this.offset = offset;
        this.length = length;
    }

    @Override
    public List<? extends SequenceSlice<T>> slice(Sequence<? extends T> sequence) {
        List<SequenceSlice<T>> ret = new LinkedList<SequenceSlice<T>>();
        int maxLength = Math.min(length, sequence.getSequenceLength() - offset);
        for (int i = 0; i < maxLength - window; i += step) {
            ret.add(new SequenceSlice<T>(sequence.getSubsequenceData(i + offset, i + offset + window), sequence, i));
        }
        for(int i=maxLength - window; i < maxLength; i += step) {
            ret.add(new SequenceSlice<T>(sequence.getSubsequenceData(i + offset, offset + maxLength), sequence, i));
        }
        return ret;
    }
}
