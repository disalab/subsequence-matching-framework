/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules.slicer;

import java.util.LinkedList;
import java.util.List;
import smf.modules.SequenceSlicer;
import smf.sequences.Sequence;
import smf.sequences.SequenceSlice;

/**
 * This slicer allows to retrieve only one subsequence from the given sequence
 * 
 * @param <T> type of internal sequence data (e.g. float[])
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class SingleSubsequenceSlicer<T> implements SequenceSlicer<T> {

    /** From which position the subsequence should start */
    protected int from;
    /** End position (exclusive) */
    protected int to;

    /**
     * Parameterless constructor. Attributes from and to are set to 0 respectively 1
     */
    public SingleSubsequenceSlicer() {
        this(0,1);
    }

    /**
     * Constructor
     * @param from offset of the subsequence to be retrieved (inclusive)
     * @param to the end of the subsequence to be retrieved (exclusive!!)
     */
    public SingleSubsequenceSlicer(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public List<? extends SequenceSlice<T>> slice(Sequence<? extends T> sequence) {
        List<SequenceSlice<T>> ret = new LinkedList<SequenceSlice<T>>();
        ret.add(new SequenceSlice<T>(sequence.getSubsequenceData(from, to), sequence, from));
        return ret;
    }

    /** This setter allows to change the behavior of this slicer
     * @param from
     * @param to
     */
    public void setFromTo(int from, int to) {
        this.from = from;
        this.to = to;
    }
}
