/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.algorithms;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.operations.data.BulkInsertOperation;
import messif.operations.query.GetObjectByLocatorOperation;
import messif.operations.query.GetRandomObjectsQueryOperation;
import messif.operations.query.KNNQueryOperation;
import smf.exceptions.StorageException;
import smf.modules.SequenceStorage;
import smf.sequences.Sequence;

/**
 * Simple algorithm for the whole sequence matching. No subsequence handling.
 * 
 * @param <T> type of internal sequence data (e.g. float[])
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class WholeSequenceMatchingAlgorithm<T, O extends Sequence<T>> extends Algorithm {
    
    /** Serial class ID for serialization */
    private static final long serialVersionUID = 97101L;

    /** Logger instance used in the whole SMF framework */
    private static final Logger logger = Logger.getLogger("SMF");
    
    /** Class of the data */
    protected final Class<O> objectClass;
    
    /** The sequence/objects storage */
    protected final SequenceStorage<T, O> wholeStorage;

    /**
     * Creates new whole sequence matching algorithm with given modules.
     * 
     * @param algorithmName name of the algorithm
     * @param objectClass class of the sequences (data + distance)
     * @param wholeStorage created type of storage
     * @throws IllegalArgumentException
     */
    @Algorithm.AlgorithmConstructor(description = "Simple algorithm for whole sequence matching", arguments = {"algorithm name", "object class", "whole gait sequence storage"})
    public WholeSequenceMatchingAlgorithm(String algorithmName, Class<O> objectClass, SequenceStorage<T, O> wholeStorage) throws IllegalArgumentException {
        super(algorithmName);
        this.objectClass = objectClass;
        this.wholeStorage = wholeStorage;
    }

    /**
     * Performs KNNQueryOperation. It returns the K best matches with respect
     * to the distance function implemented in a given sequence object (see #Constructor)
     * @param operation
     * @throws AlgorithmMethodException
     */
    public void processOperation(KNNQueryOperation operation) throws AlgorithmMethodException {        
        logger.info("KNNQueryOperation started...");
        
        wholeStorage.processQuery(operation);
        
        logger.log(Level.INFO, "Answer count: {0}", operation.getAnswerCount());
        operation.endOperation();
    }

    /**
     * Returns a bunch of random objects using an operation
     * @param operation
     */
    public void processOperation(GetRandomObjectsQueryOperation operation) {
        wholeStorage.processQuery(operation);
        operation.endOperation();
    }

    /**
     * Returns object from whole sequence data storage by it's locator - used for creating query subsequence
     *  of required offset and size (when the query is in the database).
     * @param operation
     */
    public void getSubsequenceByLocator(GetObjectByLocatorOperation operation) throws StorageException {
        LocalAbstractObject subsequence = (LocalAbstractObject) 
                wholeStorage.getSubsequence(operation.getLocator(), Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_OFFSET)), Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_OFFSET)) + Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_LENGTH)));
        operation.addToAnswer(subsequence);
        operation.endOperation();
    }
        
    /**
     * Inserts bulk of sequences to the index
     * 
     * @param operation
     * @throws AlgorithmMethodException if the storage has problems 
     */
    public void insertSequence(BulkInsertOperation operation) throws StorageException {
        int sequencesInserted = 0;                
        List<? extends LocalAbstractObject> objectList = operation.getInsertedObjects();
        logger.log(Level.INFO, "(Insert operation) Object count: {0}", operation.getInsertedObjects().size());

        for (LocalAbstractObject o: objectList) {
            try {
                wholeStorage.insertSequence((O) o);
                sequencesInserted++;
            } catch (ClassCastException ex) {
                logger.log(Level.WARNING, "Object {0} is not of type Sequence<T>: {1}", new Object[]{o, objectClass});
            }
        }
        logger.log(Level.INFO, "Sequences inserted: {0}", sequencesInserted);
        operation.endOperation();
    }

    

}
