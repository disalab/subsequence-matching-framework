/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.algorithms;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.BucketStorageException;
import messif.buckets.LocalBucket;
import messif.buckets.index.Index;
import messif.buckets.index.Search;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.operations.AnswerType;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.InsertOperation;
import messif.operations.query.GetObjectByLocatorOperation;
import messif.operations.query.GetRandomObjectsQueryOperation;
import messif.operations.query.KNNQueryOperation;
import messif.utility.reflection.NoSuchInstantiatorException;
import smf.modules.SequenceSlicer;
import smf.modules.slicer.SingleSubsequenceSlicer;
import smf.sequences.Sequence;
import smf.sequences.SequenceFactory;
import smf.utils.RankedUniqueSubsequenceParentCollection;

/**
 * This algorithm pretends, that the query sequence will have the same size, as
 * is the size of a window parameter for a slicer given for slicing indexed data.
 * So the typical use can be to slice the data with sliding slicer with let's say
 * window of size 70. Than the query sequence is expected to be of length 70.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 * 
 * @deprecated until reworked
 */
@Deprecated
public class FixedSizeQueryAlgorithm extends Algorithm {

    protected final Class<? extends Sequence> objectClass;
    protected final LocalBucket wholeStorage;
    protected final LocalBucket subStorage;
    protected final SequenceSlicer dataSlicer;
    private final int fixedWindowSize = 70;

    /**
     * Constructor
     * @param algorithmName
     * @param objectClass Any object of type Sequence
     * @param wholeStorage The sotrage structure used for whole original sequences
     * @param subStorage The storage structure for subsequences sliced from the original sequences
     * @param dataSlicer Module for slicing
     * @throws IllegalArgumentException
     */
    @Algorithm.AlgorithmConstructor(description = "Subsequence matching algorithm with fixed sized query length", arguments = {"algorithm name", "object class", "whole storage", "sub storage", "data slicer"})
    public FixedSizeQueryAlgorithm(String algorithmName, Class<? extends Sequence> objectClass, LocalBucket wholeStorage, LocalBucket subStorage, SequenceSlicer dataSlicer) throws IllegalArgumentException {
        super(algorithmName);
        this.objectClass = objectClass;
        this.wholeStorage = wholeStorage;
        this.subStorage = subStorage;
        this.dataSlicer = dataSlicer;
    }

    /**
     * Performs KNNQueryOperation. It returns the K best matches with respect
     * to the distance function implemented in a given sequence object (see #Constructor)
     * @param operation
     * @throws AlgorithmMethodException
     */
    public void processOperation(KNNQueryOperation operation) throws AlgorithmMethodException {
        RankedAbstractObject rankedAnswer;
        HashSet<String> uniqueObjects = new HashSet<String>();
        
        //ApproxKNNQueryOperation op = new ApproxKNNQueryOperation(operation.getQueryObject(), 100, AnswerType.ORIGINAL_OBJECTS, 50000, LocalSearchType.ABS_OBJ_COUNT, -1);
        KNNQueryOperation op = new KNNQueryOperation(operation.getQueryObject(), 100, AnswerType.ORIGINAL_OBJECTS);
        op.setAnswerCollection(new RankedUniqueSubsequenceParentCollection(op));
        // Perform query on subsequence storage
        subStorage.processQuery(op);
        Iterator<RankedAbstractObject> answerIterator = op.getAnswer();

        // Iterate through answers and remove duplicates
        
        while (answerIterator.hasNext()) {
            rankedAnswer = answerIterator.next();
            operation.addToAnswer((LocalAbstractObject) rankedAnswer.getObject(), rankedAnswer.getDistance(), null);
/*
            String locator = ((LocalAbstractObject) ((Sequence) rankedAnswer.getObject()).getOriginalSequence()).getLocatorURI();
            if (!uniqueObjects.contains(locator)) {
                uniqueObjects.add(locator);
                operation.addToAnswer((LocalAbstractObject) rankedAnswer.getObject(), rankedAnswer.getDistance(), null);
            }
 *
 */
        }
        // Add top k subsequences to the answer
        operation.endOperation();
    }

    /**
     * Returns object from whole sequence data storage by it's locator - used for creating query subsequence
     *  of required offset and size (when the query is in the database).
     * @param operation
     */
    public void processOperation(GetObjectByLocatorOperation operation) {

        SingleSubsequenceSlicer singleSubsequenceSlicer = new SingleSubsequenceSlicer();
        singleSubsequenceSlicer.setFromTo((Integer) operation.getParameter(Sequence.PARAM_SEQ_OFFSET), 
                (Integer) operation.getParameter(Sequence.PARAM_SEQ_OFFSET) + fixedWindowSize);
        Sequence seq = (Sequence) wholeStorage.getObject(operation.getLocator());

        LocalAbstractObject subseq;
        try {
            subseq = (LocalAbstractObject) SequenceFactory.create(seq, singleSubsequenceSlicer).get(0);
            operation.addToAnswer(subseq);
        } catch (NoSuchInstantiatorException ex) {
            Logger.getLogger(FixedSizeQueryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(FixedSizeQueryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        }

        operation.endOperation();
    }

    /**
     * Returns a bunch of random objects
     * @param operation
     */
    public void processOperation(GetRandomObjectsQueryOperation operation) {

        Index<LocalAbstractObject> index = subStorage.getIndex();
        Search<LocalAbstractObject> search = index.search();
        int positions[] = new int[operation.getCount()];
        int objCount = subStorage.getObjectCount();

        for (int i = 0; i < operation.getCount(); i++) {
            positions[i] = (int) (Math.random() * (double) objCount);
        }

        Arrays.sort(positions);

        int lastPos = 0;
        for (int i = 0; i < operation.getCount(); i++) {
            search.skip(positions[i] - lastPos);
            lastPos = positions[i];
            operation.addToAnswer(search.getCurrentObject());
        }
        operation.endOperation();
    }

    /**
     * Inserts bulk of sequences to the index
     * @param operation
     */
    public void insertSequence(BulkInsertOperation operation) {
        int sequencesInserted = 0;
        int subsequencesInserted = 0;
        List<? extends LocalAbstractObject> objectList = operation.getInsertedObjects();
        for (LocalAbstractObject o : objectList) {
            try {
                wholeStorage.addObject(o);
                sequencesInserted++;
                List<LocalAbstractObject> subList = SequenceFactory.create(objectClass.cast(o), dataSlicer);
                for (LocalAbstractObject subSeqeunce : subList) {

                    subStorage.addObject(subSeqeunce);
                    subsequencesInserted++;
                }
            } catch (NoSuchInstantiatorException ex) {
                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BucketStorageException ex) {
                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Sequences inserted: " + sequencesInserted);
        System.out.println("Subsequences inserted: " + subsequencesInserted);
        operation.endOperation();
    }

    public void insertSequence(InsertOperation operation) throws InstantiationException {

        operation.endOperation();
    }

     /**
     * Properly handles end of the work with storages
     * @throws Throwable
     */
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    @Override
    public void finalize() throws Throwable {
        if (wholeStorage != null) {
            wholeStorage.finalize();
        }
        if (subStorage != null) {
            wholeStorage.finalize();
        }
        super.finalize();
    }

    /**
     * Releases resources associated with local and sub storages
     * @throws Throwable
     */
    @Override
    public void destroy() throws Throwable {
        if (wholeStorage != null) {
            wholeStorage.destroy();
        }
        if (subStorage != null) {
            subStorage.destroy();
        }
    }

}
