/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.algorithms;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.data.BulkInsertOperation;
import messif.operations.query.GetObjectByLocatorOperation;
import messif.operations.query.GetRandomObjectsQueryOperation;
import messif.operations.query.KNNQueryOperation;
import messif.utility.reflection.NoSuchInstantiatorException;
import smf.exceptions.StorageException;
import smf.modules.SequenceStorage;
import smf.modules.slicer.SingleSubsequenceSlicer;
import smf.sequences.Sequence;
import smf.sequences.SequenceFactory;
import smf.utils.RankedUniqueSubsequenceParentCollection;

/**
 *
 * @param <T> type of internal sequence data (e.g. float[])
 * @author Jakub Valcik, Masaryk University, Brno, Czech Republic,
 * xvalcik@fi.muni.cz
 */
public class FullScanDtwAlgorithm<T, O extends Sequence<T>> extends Algorithm {

    /**
     * Serial class ID for serialization
     */
    private static final long serialVersionUID = 97301L;
    /**
     * Logger instance used in the whole SMF framework
     */
    private static final Logger logger = Logger.getLogger("SMF");
    /**
     * Class of the data
     */
    protected final O objectClass;
    /**
     * The sequence/objects storage
     */
    protected final SequenceStorage<T, O> sequenceStorage;
    private final int MIN_RANDOM_SUBSEQUENCE_LENGTH = 50;

    @Algorithm.AlgorithmConstructor(description = "Subsequence matching algorithm full scan dtw", arguments = {"sequence object class", "whole sequence storage"})
    public FullScanDtwAlgorithm(O objectClass, SequenceStorage<T, O> sequenceStorage) throws IllegalArgumentException {
        this("Subsequence matching algorithm full scan dtw", objectClass, sequenceStorage);
    }

    protected FullScanDtwAlgorithm(String algorithmName, O objectClass, SequenceStorage<T, O> sequenceStorage) throws IllegalArgumentException {
        super(algorithmName);
        this.objectClass = objectClass;
        this.sequenceStorage = sequenceStorage;
    }

    /**
     * Performs KNNQueryOperation. It returns the K best matches with respect to
     * the distance function implemented in a given sequence object
     *
     * @param operation
     * @throws StorageException
     */
    public void kNNSearch(KNNQueryOperation operation) throws StorageException {
        try {
            operation.setAnswerCollection(new RankedUniqueSubsequenceParentCollection(operation, 0.5f));

            Sequence<T> querySequence = ((Sequence<T>) operation.getQueryObject());
            logger.log(Level.FINER, "Query offset: {0}", querySequence.getOffset());

            boolean setParentSeq = Boolean.valueOf(operation.getParameter(Sequence.PARAM_SET_PARENT_SEQ, String.class, "true"));
            int dtwCount = 0;

            for (AbstractObjectIterator<LocalAbstractObject> seqIt = sequenceStorage.getAllObjects(); seqIt.hasNext();) {
                LocalAbstractObject seq = seqIt.next();
                dtwCount += dtwFullScan(operation, (Sequence<T>) seq, setParentSeq);
            }

            logger.log(Level.INFO, "Distance computation count: {0}", dtwCount);
            logger.log(Level.INFO, "Processed kNN query of length {0}", new Object[]{((Sequence) operation.getQueryObject()).getSequenceLength()});
            operation.endOperation();
        } catch (NoSuchInstantiatorException ex) {
            Logger.getLogger(VariableResultLengthAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(VariableResultLengthAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected int dtwFullScan(KNNQueryOperation operation, Sequence<T> candidateSequence, boolean setParentSeq) throws NoSuchInstantiatorException, InvocationTargetException {
        Sequence<T> querySequence = (Sequence<T>) operation.getQueryObject();
        logger.log(Level.INFO, "sequence #{1} length: {0} ", new Object[]{candidateSequence.getSequenceLength(), ((LocalAbstractObject) candidateSequence).getLocatorURI()});
        int distanceComputations = 0;
        float[][] distanceMatrix = calculateDistanceMatrix(querySequence, candidateSequence);
        for (int i = 0; i < candidateSequence.getSequenceLength(); i++) {
            ++distanceComputations;
            int window = querySequence.getSequenceLength() * 2;
            if (i + window > candidateSequence.getSequenceLength()) {
                window = candidateSequence.getSequenceLength() - i;
            }
            float[] dists = calculateShrinkingDistances(distanceMatrix, i, window);
            int index = 0;
            float dist = dists[0];
            for (int j = 1; j < dists.length; j++) {
                if (dists[j] <= dist) {
                    dist = dists[j];
                    index = j;
                }
            }
            Sequence<T> closestAnswer = new SequenceFactory(candidateSequence.getClass(), candidateSequence.getSequenceDataClass()).create(candidateSequence, i, i + index + 1, true);
            operation.addToAnswer((LocalAbstractObject) closestAnswer, dist / querySequence.getSequenceLength(), null);
        }

        return distanceComputations;
    }

    private float[][] calculateDistanceMatrix(Sequence<T> s1, Sequence<T> shrinkingSequence) {
        int n = s1.getSequenceLength(), m = shrinkingSequence.getSequenceLength();
        float[][] result = new float[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                result[i][j] = s1.getPiecewiseDist(i, shrinkingSequence, j);
            }
        }
//        int minNM = Math.min(n, m);
//        float[][] result = new float[n][m];
//        for (int i = 0; i < minNM; i++) {
//            for (int j = i; j < minNM; j++) {
//                result[i][j] = s1.getPiecewiseDist(i, shrinkingSequence, j);
//                result[j][i] = result[i][j];
//            }
//        }
//        for (int i = minNM; i < n; i++) {
//            for (int j = 0; j < m; j++) {
//                result[i][j] = s1.getPiecewiseDist(i, shrinkingSequence, j);
//            }
//        }
//        for (int j = minNM; j < m; j++) {
//            for (int i = 0; i < n; i++) {
//                result[i][j] = s1.getPiecewiseDist(i, shrinkingSequence, j);
//            }
//        }
        return result;
    }

    private float[] calculateShrinkingDistances(float[][] distanceMatrix, int shrinkingSequenceOffset, int shrinkingSequenceLength) {
        int n = distanceMatrix.length, m = shrinkingSequenceLength;
        float cm[][] = new float[n][m]; // accum matrix
        // create the accum matrix
        cm[0][0] = distanceMatrix[0][shrinkingSequenceOffset];
        for (int i = 1; i < n; i++) {
            cm[i][0] = distanceMatrix[i][shrinkingSequenceOffset] + cm[i - 1][0];
        }
        for (int j = 1; j < m; j++) {
            cm[0][j] = distanceMatrix[0][shrinkingSequenceOffset + j] + cm[0][j - 1];
        }
        // Compute the matrix values
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                // Decide on the path with minimum distance so far
                cm[i][j] = distanceMatrix[i][shrinkingSequenceOffset + j] + Math.min(cm[i - 1][j], Math.min(cm[i][j - 1], cm[i - 1][j - 1]));
            }
        }
        return cm[n - 1];
    }

    /**
     * Inserts bulk of sequences to the index. It stores both the whole sequence
     * and its subsequences to separate storage
     *
     * @param operation BulkInsertOperation
     */
    public void insertSequences(BulkInsertOperation operation) throws StorageException {
        int subsequencesInserted = 0;
        for (LocalAbstractObject o : operation.getInsertedObjects()) {
            sequenceStorage.insertSequence((O) o);
        }
        logger.log(Level.INFO, "Inserted {0} sequences slided to {1} subsequences", new Object[]{operation.getInsertedObjects().size(), subsequencesInserted});
        operation.endOperation();
    }

    /**
     * Returns a bunch of random (length and offset) subsequence objects - not a
     * priori from the stored subsequences but any from the whole sequences
     * TODO: move this into a RandomSequenceSlicer?
     *
     * @param operation
     */
    public void getRandomSubsequences(GetRandomObjectsQueryOperation operation) {
        Random random = new Random(System.currentTimeMillis());
        SingleSubsequenceSlicer singleSubsequenceSlicer = new SingleSubsequenceSlicer();

        boolean setParentSeq = Boolean.valueOf(operation.getParameter(Sequence.PARAM_SET_PARENT_SEQ, String.class, "true"));
        int minLength = Integer.valueOf(operation.getParameter(Sequence.PARAM_MIN_SEQ_LENGTH, String.class, String.valueOf(MIN_RANDOM_SUBSEQUENCE_LENGTH)));
        int maxLength = Integer.valueOf(operation.getParameter(Sequence.PARAM_MAX_SEQ_LENGTH, String.class, String.valueOf(Integer.MAX_VALUE)));

        int positions[] = new int[operation.getCount()];
        int objCount = sequenceStorage.getObjectCount();

        for (int i = 0; i < operation.getCount(); i++) {
            positions[i] = (int) (random.nextDouble() * (double) objCount);
        }

        Arrays.sort(positions);
        AbstractObjectIterator<LocalAbstractObject> allObjects = sequenceStorage.getAllObjects();

        int lastPos = 0;
        for (int i = 0; i < operation.getCount(); i++) {
            allObjects.skip(positions[i] - lastPos);
            lastPos = positions[i];
            // Generate random subsequence within the found random sequence
            Sequence randomSeq = (Sequence) allObjects.getCurrentObject();
            int randomLength = minLength + (int) (random.nextDouble() * (double) (Math.min(randomSeq.getSequenceLength(), maxLength) - minLength));
            int randomOffset = (int) (random.nextDouble() * (double) (randomSeq.getSequenceLength() - randomLength));
            singleSubsequenceSlicer.setFromTo(randomOffset, randomOffset + randomLength);
            try {
                LocalAbstractObject randomSubseq = (LocalAbstractObject) SequenceFactory.create(randomSeq, singleSubsequenceSlicer, setParentSeq).get(0);
                operation.addToAnswer(randomSubseq);
            } catch (NoSuchInstantiatorException ex) {
                Logger.getLogger(VariableQueryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(VariableQueryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Add subsequence to answer            
        }
        operation.endOperation();
    }

    /**
     * Returns object from whole sequence data storage by it's locator - used
     * for creating query subsequence of required offset and size (when the
     * query is in the database).
     *
     * @param operation
     */
    public void getSubsequenceByLocator(GetObjectByLocatorOperation operation) throws StorageException {
        LocalAbstractObject subsequence = (LocalAbstractObject) sequenceStorage.getSubsequence(operation.getLocator(), Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_OFFSET)), Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_OFFSET)) + Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_LENGTH)));
        operation.addToAnswer(subsequence);
        operation.endOperation();
    }

    public Sequence<T> getOriginalSequenceByLocator(String locator) throws StorageException {
        return sequenceStorage.getSequence(locator);
    }

    /**
     * Properly handles end of the work with storages
     *
     * @throws Throwable
     */
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    @Override
    public void finalize() throws Throwable {
        if (sequenceStorage != null) {
            sequenceStorage.finalize();
        }
        super.finalize();
    }

    /**
     * Releases resources associated with local and sub storages
     *
     * @throws Throwable
     */
    @Override
    public void destroy() throws Throwable {
        if (sequenceStorage != null) {
            sequenceStorage.destroy();
        }
    }
}
