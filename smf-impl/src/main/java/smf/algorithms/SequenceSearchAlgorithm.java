/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.algorithms;

import messif.algorithms.Algorithm;

/**
 * The simple logic of this class should wrap the whole process of any possible
 * subsequence matching algorithm. It provides only basic methods representing actions
 * that are common to all algorithms. We present a system of <tt>modules</tt>, that enables
 * the developer to customize the algorithm to his own needs.
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
@Deprecated
public class SequenceSearchAlgorithm extends Algorithm {

    public SequenceSearchAlgorithm(String algorithmName) throws IllegalArgumentException {
        super(algorithmName);
    }

    
    
//    protected ExtendedProperties configuration;
//    protected final LocalBucket wholeStorage;
//    protected final LocalBucket subStorage;
//    protected final SequenceSlicer dataSlicer;
//    protected final SequenceSlicer querySlicer;
//    protected final Class<? extends Sequence> objectClass;
//    protected static final Logger logger = Logger.getLogger(SequenceSearchAlgorithm.class.getName());
//    //protected OpearationProcessor KNNoperationProcessor;
//    //protected SequenceInsertAction insertAction;
///*
//    public SequenceSearchAlgorithm(String algorithmName) throws IllegalArgumentException, InstantiationException, IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
//    this(algorithmName, null);
//    }
//
//    public SequenceSearchAlgorithm(String algorithmName, ExtendedProperties configuration) throws IllegalArgumentException, InstantiationException, IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
//    super(algorithmName);
//    if (configuration == null) {
//    configuration = new ExtendedProperties();
//    configuration.load(new FileInputStream("/root/Devel/Java/School/db.file"));
//    }
//    this.configuration = configuration;
//    }
//     *
//     */
//
//    @Algorithm.AlgorithmConstructor(description = "Simple sequence search", arguments = {"algorithm name", "object class", "whole storage", "sub storage", "data slicer", "query slicer"})
//    public SequenceSearchAlgorithm(String algorithmName, Class<? extends Sequence> objectClass, LocalBucket wholeStorage, LocalBucket subStorage, SequenceSlicer dataSlicer, SequenceSlicer querySlicer) {
//        super(algorithmName);
//        this.objectClass = objectClass;
//        this.wholeStorage = wholeStorage;
//        this.subStorage = subStorage;
//        this.dataSlicer = dataSlicer;
//        this.querySlicer = querySlicer;
//    }
//
//    /**
//     *
//     * @param operation
//     * @throws AlgorithmMethodException
//     */
//    public void processOperation(KNNQueryOperation operation) throws AlgorithmMethodException {
//        /*
//        AbstractObjectIterator<LocalAbstractObject> allObjects = wholeStorage.getAllObjects();
//        while (allObjects.hasNext()) {
//        System.out.println(allObjects.next().toString());
//        }
//         * 
//         */
//
//        SingleSubsequenceSlicer singleSubsequenceSlicer = new SingleSubsequenceSlicer();
//        HashSet<Integer> slicesCompared = new HashSet<Integer>();
//
//        try {
//            List<LocalAbstractObject> querySubsequenceList = SequenceFactory.create(objectClass.cast(operation.getQueryObject()), querySlicer);
//            logger.log(Level.INFO, "Query sliced into {0} subsequneces", querySubsequenceList.size());
//            //SequenceFactory sequenceFactory = new SequenceFactory(objectClass, objectClass);
//            //RankedSortedCollection sortedSubStorageAnswers = new RankedSortedCollection(100, 1000);
//            //RankedSortedCollection answer = new RankedSortedCollection(operation.getK(), operation.getK());
//
//            
//            int debugI = 0;
//            int debugJ = 0;
//            int debugA = 0;
//            // For each query subsequence perform new query
//            for (LocalAbstractObject o : querySubsequenceList) {
//                debugI++;
//                debugJ = 0;
//                //if (subStorage instanceof AlgorithmStorageBucket) {
//                //logger.log(Level.INFO, "Processing subsequence {0}...", debugI);
//                ApproxKNNQueryOperation op = new ApproxKNNQueryOperation(o, 10, AnswerType.ORIGINAL_OBJECTS, 5000, LocalSearchType.ABS_OBJ_COUNT, -1f);
//                subStorage.processQuery(op);
//                Iterator<RankedAbstractObject> answerIterator = op.getAnswer();
//                Sequence subsequenceQuery = (Sequence) o;
//                // For each retrieved subsequence answer
//                while (answerIterator.hasNext()) {
//                    debugJ++;
//                    RankedAbstractObject rankedAnswer = answerIterator.next();
//                    //logger.log(Level.INFO, "Processing subsequence answer {0}, dist = " + rankedAnswer.getDistance(), debugJ);
//                    if (rankedAnswer.getObject() instanceof Sequence) {
//                        //logger.log(Level.INFO, "Aligning query sequence within the answer...");
//                        Sequence subsequenceAnswer = (Sequence) rankedAnswer.getObject();
//
//                        int sliceOffset = subsequenceAnswer.getOffset() - subsequenceQuery.getOffset();
//                        // Check whether we can align query and the slice within the answer sequence
//                        if (sliceOffset >= 0
//                                && subsequenceAnswer.getOriginalSequence().getSequenceLength() > (sliceOffset + ((Sequence) operation.getQueryObject()).getSequenceLength())) {
//                            // Eliminate duplicate answers
//                            if (!slicesCompared.contains(Integer.valueOf(sliceOffset))) {
//                                slicesCompared.add(Integer.valueOf(sliceOffset));
//                                singleSubsequenceSlicer.setFromTo(sliceOffset, sliceOffset + ((Sequence) operation.getQueryObject()).getSequenceLength());
//                                Sequence alignedOriginalSubsequence = (Sequence) SequenceFactory.create(subsequenceAnswer.getOriginalSequence(), singleSubsequenceSlicer).get(0);
//                                // Distance between the query sequence and aligned subsequence sliced from the answer subsequence's original sequence
//                                float dist = ((LocalAbstractObject) alignedOriginalSubsequence).getDistance(operation.getQueryObject());
//                                //logger.log(Level.FINE, "Adding sequence to the answer collection...");
//                                //System.out.println("Adding sequence to the answer collection...");
//                                operation.addToAnswer((LocalAbstractObject) alignedOriginalSubsequence, dist, null);
//                                //answer.add(new RankedAbstractObject((LocalAbstractObject) alignedOriginalSubsequence, dist));
//                                debugA++;
//                            }
//                        }
//                    }
//                }
//                //}
//            }
//            // Return k best answers form the priority queue
//            //logger.log(Level.INFO, "Searching done, "+debugA+" answers added");
//            //operation.setAnswerCollection(answer);
//        } catch (NoSuchInstantiatorException ex) {
//            Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (InvocationTargetException ex) {
//            Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        operation.endOperation();
//    }
//
//    /**
//     * Inserts single sequence and it's subsequences
//     * @param operation
//     * @throws InstantiationException
//     */
//    public void insertSequence(InsertOperation operation) throws InstantiationException {
//        if (!(operation.getInsertedObject() instanceof Sequence)) {
//            throw new InstantiationException("Inserted object must implement Sequence interface!");
//        } else {
//            try {
//                wholeStorage.addObject(operation.getInsertedObject());
//                List<LocalAbstractObject> subList = SequenceFactory.create(objectClass.cast(operation.getInsertedObject()), dataSlicer);
//                for (LocalAbstractObject o : subList) {
//                    subStorage.addObject(o);
//                }
//            } catch (NoSuchInstantiatorException ex) {
//                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InvocationTargetException ex) {
//                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (BucketStorageException ex) {
//                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        operation.endOperation();
//    }
//
//    public void insertSequence(BulkInsertOperation operation) {
//        List<? extends LocalAbstractObject> objectList = operation.getInsertedObjects();
//        for (LocalAbstractObject o : objectList) {
//            try {
//                wholeStorage.addObject(o);
//                List<LocalAbstractObject> subList = SequenceFactory.create(objectClass.cast(o), dataSlicer);
//                for (LocalAbstractObject subSeqeunce : subList) {
//                    subStorage.addObject(subSeqeunce);
//                }
//            } catch (NoSuchInstantiatorException ex) {
//                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InvocationTargetException ex) {
//                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (BucketStorageException ex) {
//                Logger.getLogger(SequenceSearchAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        operation.endOperation();
//    }
//
//    public void deleteSequence(DeleteOperation operation) {
//    }
//
//    @Override
//    public String toString() {
//        String ret = "";
//        ret += "Simple silly sequence search algorithm\n";
//        ret += "Stored sequences: " + wholeStorage.getObjectCount() + "\n";
//        ret += "Stored sequences: " + subStorage.getObjectCount() + "\n";
//        return ret;
//    }
}
