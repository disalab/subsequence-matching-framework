/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.algorithms;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.operations.query.GetObjectByLocatorOperation;
import messif.operations.query.KNNQueryOperation;
import messif.utility.reflection.NoSuchInstantiatorException;
import smf.exceptions.StorageException;
import smf.modules.DistanceIndex;
import smf.modules.SequenceSlicer;
import smf.modules.SequenceStorage;
import smf.modules.slicer.AllPossibleWindowsSlicer;
import smf.modules.slicer.SlidingSubsequenceSlicer;
import smf.sequences.DistanceAllowsNonEquilength;
import smf.sequences.Sequence;
import smf.sequences.SequenceFactory;
import smf.utils.RankedUniqueSubsequenceParentCollection;

/**
 * This algorithm provides functionality for the subsequence matching with
 * variable query length on the original data. Post processing with {@link smf.modules.slicer.AllPossibleWindowsSlicer}.
 *
 * TODO: allow to change postprocess slicer
 *
 * @param <T> type of internal sequence data (e.g. float[])
 * @author Jakub Valcik, Masaryk University, Brno, Czech Republic,
 * xvalcik@fi.muni.cz
 */
public class VariableResultLengthAlgorithm<T, O extends Sequence<T>> extends VariableQueryAlgorithm<T, O> {

    /**
     * Serial class ID for serialization
     */
    private static final long serialVersionUID = 97301L;
    /**
     * Logger instance used in the whole SMF framework
     */
    private static final Logger logger = Logger.getLogger("SMF");

    @Algorithm.AlgorithmConstructor(description = "Subsequence matching algorithm with variable result length", arguments = {"sequence object class", "whole sequence storage", "subsequence storage", "data slicer", "query slicer", "window size"})
    public VariableResultLengthAlgorithm(Class<O> objectClass, SequenceStorage<T, O> sequenceStorage,
            DistanceIndex distanceIndex, SequenceSlicer dataSlicer, SequenceSlicer querySlicer, int windowSize) throws IllegalArgumentException {
        this(objectClass, sequenceStorage, distanceIndex, dataSlicer, querySlicer, windowSize, 100);
    }

    @Algorithm.AlgorithmConstructor(description = "Subsequence matching algorithm with variable query length", arguments = {"sequence object class", "whole sequence storage", "subsequence storage", "data slicer", "query slicer", "window size", "# of candidate slices"})
    public VariableResultLengthAlgorithm(Class<O> objectClass, SequenceStorage<T, O> sequenceStorage,
            DistanceIndex distanceIndex, SequenceSlicer dataSlicer, SequenceSlicer querySlicer, int windowSize, int kNNSlices) throws IllegalArgumentException {
        super("Subsequence matching algorithm with variable result length", objectClass, sequenceStorage, distanceIndex, dataSlicer, querySlicer, windowSize, kNNSlices);
    }

    /**
     * Performs KNNQueryOperation. It returns the K best matches with respect to
     * the distance function implemented in a given sequence object
     *
     * @param operation
     * @throws StorageException
     */
    @Override
    public void kNNSearch(KNNQueryOperation operation) throws StorageException {
        try {
            operation.setAnswerCollection(new RankedUniqueSubsequenceParentCollection(operation));

            Sequence<T> querySequence = ((Sequence<T>) operation.getQueryObject());
            logger.log(Level.FINER, "Query offset: {0}", querySequence.getOffset());
            boolean allowsNonEquilength = (operation.getQueryObject() instanceof DistanceAllowsNonEquilength);
            boolean setParentSeq = Boolean.valueOf(operation.getParameter(Sequence.PARAM_SET_PARENT_SEQ, String.class, "true"));

            List<LocalAbstractObject> querySubsequenceList = SequenceFactory.create(objectClass.cast(operation.getQueryObject()), querySlicer);
            Map<String, TreeSet<Integer>> sequenceIntervalMap = new HashMap<String, TreeSet<Integer>>();

            // For each query subsequence run a separate query
            for (LocalAbstractObject o : querySubsequenceList) {

                Iterator<RankedAbstractObject> answerIterator = distanceIndex.findNearest(o, kNNSlices);
                Sequence querySubsequence = (Sequence) o;
                logger.log(Level.FINER, "Query slice offset: {0}", querySubsequence.getOffset());

                // For each retrieved subsequence answer
                while (answerIterator.hasNext()) {
                    Sequence<T> subseqAnswer = (Sequence<T>) answerIterator.next().getObject();

                    if (!sequenceIntervalMap.containsKey(subseqAnswer.getOriginalSequenceLocator())) {
                        sequenceIntervalMap.put(subseqAnswer.getOriginalSequenceLocator(), new TreeSet<Integer>());
                    }


                    int sliceOffset = subseqAnswer.getOffset() - 2 * (querySubsequence.getOffset() + querySubsequence.getSequenceLength() / 2);
                    if (sliceOffset < 0) {
                        sliceOffset = 0;
                    }
                    sequenceIntervalMap.get(subseqAnswer.getOriginalSequenceLocator()).add(sliceOffset);
                }
            }

            logger.log(Level.INFO, "Candidate sequences count: {0}", sequenceIntervalMap.size());

            int intervalsCount = 0;
            int dtwCount = 0;

            for (Iterator<String> candidateSequenceIt = sequenceIntervalMap.keySet().iterator(); candidateSequenceIt.hasNext();) {
                String candidateSequenceLocator = candidateSequenceIt.next();
                Sequence<T> candidateSequence = sequenceStorage.getSequence(candidateSequenceLocator);

                Iterator<Integer> offsetIt = sequenceIntervalMap.get(candidateSequenceLocator).iterator();
                int startOffset = offsetIt.next();
                int subseqTo = startOffset + 2 * querySequence.getSequenceLength();
                while (offsetIt.hasNext()) {
                    int offset = offsetIt.next();
                    if (offset > subseqTo) {
                        ++intervalsCount;
                        dtwCount += examineInterval(operation, candidateSequence, startOffset, subseqTo, allowsNonEquilength, setParentSeq);
                        startOffset = offset;
                    }
                    subseqTo = offset + 2 * querySequence.getSequenceLength();
                }
                ++intervalsCount;
                dtwCount += examineInterval(operation, candidateSequence, startOffset, subseqTo, allowsNonEquilength, setParentSeq);
            }
            logger.log(Level.INFO, "Examined intervals count: {0}", intervalsCount);
            logger.log(Level.INFO, "Distance computation count: {0}", dtwCount);
            logger.log(Level.INFO, "Processed kNN query of length {0}, window size = {1}\n\tthe query initiated {2} subsequent queries to the distane index", new Object[]{((Sequence) operation.getQueryObject()).getSequenceLength(), windowSize, querySubsequenceList.size()});
            operation.endOperation();
        } catch (NoSuchInstantiatorException ex) {
            Logger.getLogger(VariableResultLengthAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(VariableResultLengthAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected int examineInterval(KNNQueryOperation operation, Sequence<T> candidateSequence, int offset, int to, boolean allowsNonEquilength, boolean setParentSeq) throws NoSuchInstantiatorException, InvocationTargetException {
        if (to > candidateSequence.getSequenceLength()) {
            to = candidateSequence.getSequenceLength();
        }
        Sequence<T> querySequence = (Sequence<T>) operation.getQueryObject();
        SlidingSubsequenceSlicer slidingSlicer = new SlidingSubsequenceSlicer(querySequence.getSequenceLength() * 2, 1, offset, to - offset);
        List<Sequence<T>> subsequenceSlidingSlices = SequenceFactory.create(objectClass.cast(candidateSequence), slidingSlicer, setParentSeq);
        logger.log(Level.INFO, "Sliding Slices count: {0} [{1} - {2}] #{3}", new Object[]{subsequenceSlidingSlices.size(), offset, to, ((LocalAbstractObject) candidateSequence).getLocatorURI()});
        int distanceComputations = 0;
        for (int i = 0; i < subsequenceSlidingSlices.size(); i++) {
            ++distanceComputations;
            float[] dists = calculateShrinkingDistances(querySequence, subsequenceSlidingSlices.get(i));
            int index = 0;
            float dist = dists[0];
            for (int j = 1; j < dists.length; j++) {
                if (dists[j] <= dist) {
                    dist = dists[j];
                    index = j;
                }
            }
            Sequence<T> closestAnswer = new SequenceFactory(candidateSequence.getClass(), candidateSequence.getSequenceDataClass()).create(candidateSequence, offset + i, offset + i + index + 1, true);
            operation.addToAnswer((LocalAbstractObject) closestAnswer, dist, null);
        }

        return distanceComputations;
    }

    private float[] calculateShrinkingDistances(Sequence<T> s1, Sequence<T> shrinkingSequence) {
        int n = s1.getSequenceLength(), m = shrinkingSequence.getSequenceLength();
        float cm[][] = new float[n][m]; // accum matrix
        // create the accum matrix
        cm[0][0] = s1.getPiecewiseDist(0, shrinkingSequence, 0);
        for (int i = 1; i < n; i++) {
            cm[i][0] = s1.getPiecewiseDist(i, shrinkingSequence, 0) + cm[i - 1][0];
        }
        for (int j = 1; j < m; j++) {
            cm[0][j] = s1.getPiecewiseDist(0, shrinkingSequence, j) + cm[0][j - 1];
        }
        // Compute the matrix values
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                // Decide on the path with minimum distance so far
                cm[i][j] = s1.getPiecewiseDist(i, shrinkingSequence, j) + Math.min(cm[i - 1][j], Math.min(cm[i][j - 1], cm[i - 1][j - 1]));
            }
        }
        return cm[n - 1];
    }

    /**
     * Returns object from whole sequence data storage by it's locator - used
     * for creating query subsequence of required offset and size (when the
     * query is in the database).
     *
     * @param operation
     */
    public Sequence<T> getOriginalSequenceByLocator(String locator) throws StorageException {
        return sequenceStorage.getSequence(locator);
    }

    /**
     * Properly handles end of the work with storages
     *
     * @throws Throwable
     */
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    @Override
    public void finalize() throws Throwable {
        super.finalize();
    }

    /**
     * Releases resources associated with local and sub storages
     *
     * @throws Throwable
     */
    @Override
    public void destroy() throws Throwable {
        super.destroy();
    }
}
