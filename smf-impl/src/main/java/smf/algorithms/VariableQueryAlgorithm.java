/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.algorithms;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.RankedAbstractObject;
import messif.operations.data.BulkInsertOperation;
import messif.operations.query.GetObjectByLocatorOperation;
import messif.operations.query.GetRandomObjectsQueryOperation;
import messif.operations.query.KNNQueryOperation;
import messif.utility.reflection.NoSuchInstantiatorException;
import smf.exceptions.StorageException;
import smf.modules.DistanceIndex;
import smf.modules.SequenceSlicer;
import smf.modules.SequenceStorage;
import smf.modules.slicer.SingleSubsequenceSlicer;
import smf.sequences.DistanceAllowsNonEquilength;
import smf.sequences.Sequence;
import smf.sequences.SequenceFactory;
import smf.utils.RankedUniqueSubsequenceParentCollection;

/**
 * This algorithm provides functionality for the subsequence matching with variable query length on the original data.
 * 
 * @param <T> type of internal sequence data (e.g. float[])
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class VariableQueryAlgorithm<T, O extends Sequence<T>> extends Algorithm {

    /** Serial class ID for serialization */
    private static final long serialVersionUID = 97301L;

    /** Logger instance used in the whole SMF framework */
    private static final Logger logger = Logger.getLogger("SMF");
    
    /** Class of the data */
    protected final Class<O> objectClass;

    /** Slicer to be applied on the data sequences */
    protected final SequenceSlicer dataSlicer;
    
    /** Slicer to be applied on the query sequence. */
    protected final SequenceSlicer querySlicer;
    
    /** The sequence/objects storage */
    protected final SequenceStorage<T, O> sequenceStorage;

    /** Distance index to store and retrieve subsequences according to a distance function */
    protected final DistanceIndex distanceIndex;
    
    /** Size of the data and query slices */
    protected final int windowSize;
    
    /** Number of nearest neighbors retrieved for each query subsequence */
    protected int kNNSlices;

    @Algorithm.AlgorithmConstructor(description = "Subsequence matching algorithm with variable query length", arguments = {"sequence object class", "whole sequence storage", "subsequence storage", "data slicer", "query slicer", "window size"})
    public VariableQueryAlgorithm(Class<O> objectClass, SequenceStorage<T, O> sequenceStorage, 
            DistanceIndex distanceIndex, SequenceSlicer dataSlicer, SequenceSlicer querySlicer, int windowSize) throws IllegalArgumentException {
        this(objectClass, sequenceStorage, distanceIndex, dataSlicer, querySlicer, windowSize, 100);
    }
    
    @Algorithm.AlgorithmConstructor(description = "Subsequence matching algorithm with variable query length", arguments = {"sequence object class", "whole sequence storage", "subsequence storage", "data slicer", "query slicer", "window size", "# of candidate slices"})
    public VariableQueryAlgorithm(Class<O> objectClass, SequenceStorage<T, O> sequenceStorage, 
            DistanceIndex distanceIndex, SequenceSlicer dataSlicer, SequenceSlicer querySlicer, int windowSize, int kNNSlices) throws IllegalArgumentException {
        this("Subsequence matching algorithm with variable query length", objectClass, sequenceStorage, distanceIndex, dataSlicer, querySlicer, windowSize, kNNSlices);
    }
    
    protected VariableQueryAlgorithm(String algorithmName, Class<O> objectClass, SequenceStorage<T, O> sequenceStorage, 
            DistanceIndex distanceIndex, SequenceSlicer dataSlicer, SequenceSlicer querySlicer, int windowSize, int kNNSlices) throws IllegalArgumentException {
        super(algorithmName);
        this.objectClass = objectClass;
        this.dataSlicer = dataSlicer;
        this.querySlicer = querySlicer;
        this.distanceIndex = distanceIndex;
        this.sequenceStorage = sequenceStorage;
        this.windowSize = windowSize;
        this.kNNSlices = kNNSlices;
    }

    /**
     * Performs KNNQueryOperation. It returns the K best matches with respect
     * to the distance function implemented in a given sequence object
     * 
     * @param operation
     * @throws StorageException
     */
    public void kNNSearch(KNNQueryOperation operation) throws StorageException {
        try {
            operation.setAnswerCollection(new RankedUniqueSubsequenceParentCollection(operation));

            Sequence<T> querySequence = ((Sequence<T>) operation.getQueryObject());
            boolean allowsNonEquilength = (operation.getQueryObject() instanceof DistanceAllowsNonEquilength);
            boolean setParentSeq = Boolean.valueOf(operation.getParameter(Sequence.PARAM_SET_PARENT_SEQ, String.class, "true"));
            
            List<LocalAbstractObject> querySubsequenceList = SequenceFactory.create(objectClass.cast(operation.getQueryObject()), querySlicer);

            // For each query subsequence run a separate query
            for (LocalAbstractObject o : querySubsequenceList) {
                Iterator<RankedAbstractObject> answerIterator = distanceIndex.findNearest(o, kNNSlices);
                Sequence querySubsequence = (Sequence) o;
                
                // For each retrieved subsequence answer
                while (answerIterator.hasNext()) {                    
                    Sequence<T> subseqAnswer = (Sequence<T>) answerIterator.next().getObject();

                    int sliceOffset = subseqAnswer.getOffset() - querySubsequence.getOffset();
                    if (sliceOffset < 0 && !allowsNonEquilength) {
                        continue;
                    }                    
                    Sequence<T> alignedDataSubsequence = sequenceStorage.getSubsequence(subseqAnswer.getOriginalSequenceLocator(), sliceOffset, sliceOffset + querySequence.getSequenceLength(), setParentSeq);
                    if (alignedDataSubsequence.getSequenceLength() < querySequence.getSequenceLength() && !allowsNonEquilength) {
                        continue;
                    }
                    // Distance between the query sequence and aligned subsequence sliced from the answer subsequence's original sequence
                    float dist = ((LocalAbstractObject) alignedDataSubsequence).getDistance(operation.getQueryObject());
                    //logger.log(Level.FINE, "Adding sequence to the answer collection...");
                    operation.addToAnswer((LocalAbstractObject) alignedDataSubsequence, dist, null);
                }                
            }
            logger.log(Level.INFO, "Processed kNN query of length {0}, window size = {1}\n\tthe query initiated {2} subsequent queries to the distane index", new Object[] {((Sequence) operation.getQueryObject()).getSequenceLength(), windowSize, querySubsequenceList.size()});
            operation.endOperation();
        } catch (NoSuchInstantiatorException ex) {
            Logger.getLogger(VariableQueryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(VariableQueryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Inserts bulk of sequences to the index. It stores both the whole sequence
     * and its subsequences to separate storage
     * 
     * @param operation BulkInsertOperation
     */
    public void insertSequences(BulkInsertOperation operation) throws StorageException {
        int subsequencesInserted = 0;
        for (LocalAbstractObject o : operation.getInsertedObjects()) {
            if (((Sequence<T>) o).getSequenceLength() < windowSize) {
                log.log(Level.WARNING, "sequence with ID + {0} is shorter than window size: {1}. Sequence is not stored.", new Object[]{o.getLocatorURI(), ((Sequence<T>) o).getSequenceLength()});
                continue;
            }
            sequenceStorage.insertSequence((O) o);
            subsequencesInserted += distanceIndex.sliceAndAdd(objectClass.cast(o), dataSlicer);
        }
        logger.log(Level.INFO, "Inserted {0} sequences slided to {1} subsequences", new Object[] {operation.getInsertedObjects().size(), subsequencesInserted});
        operation.endOperation();
    }

    /**
     * Returns a bunch of random (length and offset) subsequence objects - not a priori from the 
     *   stored subsequences but any from the whole sequences
     * TODO: move this into a RandomSequenceSlicer?
     * 
     * @param operation
     */
    public void getRandomSubsequences(GetRandomObjectsQueryOperation operation) {
        Random random = new Random(System.currentTimeMillis());
        SingleSubsequenceSlicer singleSubsequenceSlicer = new SingleSubsequenceSlicer();
        
        boolean setParentSeq = Boolean.valueOf(operation.getParameter(Sequence.PARAM_SET_PARENT_SEQ, String.class, "true"));
        int minLength = Integer.valueOf(operation.getParameter(Sequence.PARAM_MIN_SEQ_LENGTH, String.class, String.valueOf(windowSize)));
        int maxLength = Integer.valueOf(operation.getParameter(Sequence.PARAM_MAX_SEQ_LENGTH, String.class, String.valueOf(Integer.MAX_VALUE)));
        
        int positions[] = new int[operation.getCount()];
        int objCount = sequenceStorage.getObjectCount();

        for (int i = 0; i < operation.getCount(); i++) {
            positions[i] = (int) (random.nextDouble() * (double) objCount);
        }

        Arrays.sort(positions);
        AbstractObjectIterator<LocalAbstractObject> allObjects = sequenceStorage.getAllObjects();
        
        int lastPos = 0;
        for (int i = 0; i < operation.getCount(); i++) {
            allObjects.skip(positions[i] - lastPos);
            lastPos = positions[i];
            // Generate random subsequence within the found random sequence
            Sequence randomSeq = (Sequence) allObjects.getCurrentObject();
            int randomLength = minLength + (int) (random.nextDouble() * (double) (Math.min(randomSeq.getSequenceLength(), maxLength) - minLength));
            int randomOffset = (int) (random.nextDouble() * (double) (randomSeq.getSequenceLength() - randomLength));
            singleSubsequenceSlicer.setFromTo(randomOffset, randomOffset + randomLength);
            try {
                LocalAbstractObject randomSubseq = (LocalAbstractObject) SequenceFactory.create(randomSeq, singleSubsequenceSlicer, setParentSeq).get(0);
                operation.addToAnswer(randomSubseq);
            } catch (NoSuchInstantiatorException ex) {
                Logger.getLogger(VariableQueryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(VariableQueryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Add subsequence to answer            
        }
        operation.endOperation();
    }

    /**
     * Returns object from whole sequence data storage by it's locator - used for creating query subsequence
     *  of required offset and size (when the query is in the database).
     * @param operation
     */
    public void getSubsequenceByLocator(GetObjectByLocatorOperation operation) throws StorageException {
        LocalAbstractObject subsequence = (LocalAbstractObject) 
                sequenceStorage.getSubsequence(operation.getLocator(), Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_OFFSET)), Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_OFFSET)) + Integer.valueOf((String) operation.getParameter(Sequence.PARAM_SEQ_LENGTH)));
        operation.addToAnswer(subsequence);
        operation.endOperation();
    }
    
    /**
     * Properly handles end of the work with storages
     * @throws Throwable
     */
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    @Override
    public void finalize() throws Throwable {
        if (sequenceStorage != null) {
            sequenceStorage.finalize();
        }
        if (distanceIndex != null) {
            sequenceStorage.finalize();
        }
        super.finalize();
    }

    /**
     * Releases resources associated with local and sub storages
     * @throws Throwable
     */
    @Override
    public void destroy() throws Throwable {
        if (sequenceStorage != null) {
            sequenceStorage.destroy();
        }
        if (distanceIndex != null) {
            distanceIndex.destroy();
        }
    }
}
