/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.graphics;



import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import smf.sequence.datatypes.GaitDistanceTimeRepresentation;
import smf.sequence.impl.GaitSimiMotionObject;
import smf.sequences.RenderableSequence;

/**
 * This class provides static methods for generating html5 canvas code that
 * visualizes gait objects
 * TODO: clean and comment
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class GaitCanvasCreator {

    private float minVal;
    private float maxVal;
    private int yRange;
    private int xRange;
    private static int subId = 0;
    private String colors[] = {"#F00", "#FF0", "#0FF", "#F0F", "#F00", "#00F"};

    public GaitCanvasCreator() {
    }

    /**
     * Constructor for filling basic rendering parameters
     * @param minVal Minimum value that is visualizable in this canvas
     * @param maxVal Maximum value that is visualizable in this canvas
     * @param yRange Height of the canvas in pixels
     */
    public GaitCanvasCreator(double minVal, double maxVal, int yRange) {
        this.minVal = (float) minVal;
        this.maxVal = (float) maxVal;
        this.yRange = yRange;        
    }

    /**
     * Generates Html5 canvas tag
     * @param obj LocalAbstractObject to be rendered
     * @return String repesenting Html5 canvas code that renders the given sequence
     */
    public String generateCanvas(LocalAbstractObject obj) {
        GaitSimiMotionObject<GaitDistanceTimeRepresentation> gObj = (GaitSimiMotionObject<GaitDistanceTimeRepresentation>) obj;
        RenderableSequence objToRender = (RenderableSequence) gObj;


        //Sequence seqObj = (Sequence) obj;
        subId++;
        String seqId = gObj.getLocatorURI();
        //String htmlSeqId = seqId + "." + subId;
        //int subsequenceOffset = seqObj.getOffset();
        xRange = gObj.getSequenceLength();

        String res = "";
        res += "<div class=\"container\" style=\"width: " + xRange + "px\">\n";
        res += "  " + getCanvasTag("sequence." + subId, "sequence", xRange, yRange) + "HTML5 Canvas not suported</canvas>\n";
        //res += "  " + getCanvasTag("subsequence." + htmlSeqId, "subsequence", width, minHeight) + "HTML5 Canvas not suported</canvas><br/>\n";
        res += "  <center>\n";
        //res += "  " + getRangeTag(htmlSeqId, width, height, subsequenceSize, subsequenceOffset) + "<br/>\n";
        res += "  " + getSpanTag(subId+"", seqId) + "<br/>\n";
        res += "  " + getSearchLink(seqId) + "\n";
        //res += "  " + getSearchLink(htmlSeqId, subsequenceOffset) + "\n";
        res += "  </center>\n";
        res += "  <script type=\"text/javascript\">\n";
        //res += "  var c=document.getElementById(\"sequence." + htmlSeqId + "\");\n";

        //Draw sequences
        res += "  var c=document.getElementById(\"sequence." + subId + "\");\n";
        res += "  var ctx=c.getContext(\"2d\");\n";
        int c = 0;
        for (int i = 0; i < objToRender.getDataForRendering().length; i++) {
            res += drawSequence(objToRender.getDataForRendering()[i], 0, colors[c]);
            c++;
            if (c > colors.length) {
                c = 0;
            }
        }        

        res += "  </script>\n";
        res += "</div><br>";
        return res;
    }

    
    /**
     * Generates Html5 canvas tag
     * @param obj LocalAbstractObject to be rendered
     * @return String repesenting Html5 canvas code that renders the given sequence
     */
    public String generateCanvas(RankedAbstractObject answer, LocalAbstractObject queryObject) {
        float dist = answer.getDistance();
        GaitSimiMotionObject<GaitDistanceTimeRepresentation> gObj = (GaitSimiMotionObject<GaitDistanceTimeRepresentation>) answer.getObject();
        GaitSimiMotionObject<GaitDistanceTimeRepresentation> qObj = (GaitSimiMotionObject<GaitDistanceTimeRepresentation>) queryObject;
        RenderableSequence objToRender = (RenderableSequence) gObj;
        RenderableSequence queryToRender = (RenderableSequence) qObj;

        subId++;
        String seqId = gObj.getLocatorURI();        
        //String htmlSeqId = seqId + "." + subId;
        //int subsequenceOffset = seqObj.getOffset();
        xRange = Math.max(gObj.getSequenceLength(), qObj.getSequenceLength());

        String res = "";
        res += "<div class=\"container\" style=\"width: " + xRange + "px\">\n";
        res += "  " + getCanvasTag("sequence." + subId, "sequence", xRange, yRange) + "HTML5 Canvas not suported</canvas>\n";
        //res += "  " + getCanvasTag("subsequence." + htmlSeqId, "subsequence", width, minHeight) + "HTML5 Canvas not suported</canvas><br/>\n";
        res += "  <center>\n";
        //res += "  " + getRangeTag(htmlSeqId, width, height, subsequenceSize, subsequenceOffset) + "<br/>\n";
        res += "  " + getSpanTag(subId+"", String.valueOf(dist)) + "<br/>\n";
        res += "  " + getSpanTag(subId+"", seqId) + "<br/>\n";
        res += "  " + getSearchLink(seqId) + "\n";
        res += "  </center>\n";
        res += "  <script type=\"text/javascript\">\n";
        //res += "  var c=document.getElementById(\"sequence." + htmlSeqId + "\");\n";
        
        //Draw sequences
        res += "  var c=document.getElementById(\"sequence." + subId + "\");\n";
        res += "  var ctx=c.getContext(\"2d\");\n";
        int c = 0;
        for (int i = 0; i < objToRender.getDataForRendering().length; i++) {
            res += drawSequence(objToRender.getDataForRendering()[i], 0, colors[c]);
            res += drawSequence(queryToRender.getDataForRendering()[i], 0, "#000");
            c++;
            if (c > colors.length) {
                c = 0;
            }
        }        

        res += "  </script>\n";
        res += "</div>";
        return res;
    }

    /**
     * Generates Html5 code representing one float sequence. The given sequence starts
     * on the x position of the canvas determined by the fromX parameter.
     * @param seq Sequence to be rendered
     * @param Offset X position within the canvas from where the rendering should start
     * @param color Color of the sequence in hexadecimal notation (like #F00 or #FF0000)
     * @return Html5 canvas code just for the rendering of the given sequence (not the whole canvas tag)
     */
    private String drawSequence(float[] seq, int fromX, String color) {
        int[] normData = normalizeData(seq);
        String res = "";        
        res += "  ctx.strokeStyle = '" + color + "';\n";
        res += "  ctx.lineWidth = 2;\n";
        res += "  ctx.beginPath();\n";
        // First point
        res += "    ctx.moveTo(" + fromX + ", " + (yRange - normData[0]) + ");\n";
        for (int i = 1; i < seq.length; i++) {
            res += "    ctx.lineTo(" + (i + fromX) + "," + (yRange - normData[i]) + ");\n";
        }
        res += "    ctx.stroke();\n";
        res += "  ctx.closePath();\n";
        return res;
    }

    private String getCanvasTag(String idTag, String classTag, int width, int height) {
        return "<canvas id=\"" + idTag + "\" class=\"" + classTag + "\" width=\"" + width + "\" height=\"" + height + "\">";
    }

    private String getRangeTag(String subSeqId, int width, int height, int subsequenceSize, int currValue) {
        return "<input type=\"range\" id=\"slider" + subSeqId + "\" style=\"width: " + (width - subsequenceSize + 10) + "px\" "
                + "min=\"0\" max=\"" + (width - subsequenceSize) + "\" step=\"1\" value=\"" + currValue + "\""
                + " onchange=\"updateSubsequence('" + subSeqId + "', this.value, " + subsequenceSize + ", " + width + ", " + height + ")\">";
    }

    private String getSpanTag(String id, String seqId) {
        return "<span id=\"currValue" + id + "\">"+seqId+"</span>";
    }

        private static String getSearchLink(String seqId) {
        String res = "";
        //String seqId = htmlSeqId.split("\\.")[0];
        res += "<a href=\"similar?locator=" + seqId + "\" id=\"search." + seqId + "\">Find similar sequences</a>";
        return res;
    }

    /**
     * Normalizes the given raw data and fits them within the drawing rectangle
     * @param data float array to be normalized
     * @return Normalized values that can be used as a coordinates of points in the canvas
     */
    private int[] normalizeData(float[] data) {
        int[] newData = new int[data.length];
        float range = maxVal - minVal;
        float floor = 0 - minVal;
        float yScale = (float) yRange / range;
        float normVal;

        // Make all values positivie and adjust all the values according to the yScale
        for (int i = 0; i < data.length; i++) {
            normVal = data[i] + floor;
            normVal *= yScale;
            newData[i] = (int) normVal;
        }
        return newData;
    }

    /**
     * Finds minimum value in the given array
     * @param data
     * @return
     */
    private float getMinValue(float[] data) {
        float bestSoFar = data[1]; // data[1] because we want to omit the first value
        for (int i = 1; i < data.length; i++) {
            if (data[i] < bestSoFar) {
                bestSoFar = data[i];
            }
        }
        return bestSoFar;
    }

    /**
     * Finds maximum value in the given array
     * @param data
     * @return
     */
    private float getMaxValue(float[] data) {
        float bestSoFar = data[1]; // data[1] because we want to omit the first value
        for (int i = 1; i < data.length; i++) {
            if (data[i] > bestSoFar) {
                bestSoFar = data[i];
            }
        }
        return bestSoFar;
    }
}
