/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.objects.util.RankedAbstractObject;
import messif.objects.util.RankedSortedCollection;
import messif.operations.RankingQueryOperation;
import smf.sequences.Sequence;

/**
 * This collections maintains only such objects (subsequences) that are unique,
 * which means either they are from different parent sequence or they do not
 * overlap.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic,
 * david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic,
 * volny.petr@gmail.com
 * @author Jakub Valcik, Masaryk University, Brno, Czech Republic,
 * xvalcik@fi.muni.cz
 */
public class RankedUniqueSubsequenceParentCollection extends RankedSortedCollection {

    private final float allowedRelativeOverlap;

    public RankedUniqueSubsequenceParentCollection() throws IllegalArgumentException {
        this(0);
    }

    public RankedUniqueSubsequenceParentCollection(float allowedRelativeOverlap) throws IllegalArgumentException {
        super();
        this.allowedRelativeOverlap = allowedRelativeOverlap < 0 ? 0 : (allowedRelativeOverlap > 1 ? 1 : allowedRelativeOverlap);
    }

    public RankedUniqueSubsequenceParentCollection(RankingQueryOperation operation) {
        this(operation, 0);
    }

    public RankedUniqueSubsequenceParentCollection(RankingQueryOperation operation, float allowedRelativeOverlap) {
        super(operation);
        this.allowedRelativeOverlap = allowedRelativeOverlap < 0 ? 0 : (allowedRelativeOverlap > 1 ? 1 : allowedRelativeOverlap);
    }

    /**
     * Adds an object (sequence) only if this sequence (overlapping) is not in
     * the collection already.
     *
     * @param e RankedAbstractObject to be added to the answer
     * @return true if the RankedAbstractObject was added to the answer
     */
    @Override
    public boolean add(RankedAbstractObject e) {
        Sequence eSeq = (Sequence) e.getObject();
        if (eSeq.getOriginalSequenceLocator() == null) {
            Logger.getLogger(RankedUniqueSubsequenceParentCollection.class.getName()).log(Level.SEVERE, "OriginalSequenceLocator is NULL");
            return false;
        }

        List<RankedAbstractObject> toRemove = new ArrayList<RankedAbstractObject>();
        // Iterate through the objects added to the answer so far
        for (Iterator<RankedAbstractObject> it = this.iterator(); it.hasNext();) {
            RankedAbstractObject itObj = it.next();
            Sequence itObjSeq = (Sequence) itObj.getObject();
            if (itObjSeq.getOriginalSequenceLocator().equals(eSeq.getOriginalSequenceLocator())) {
                // Find out whether the sequences overlaping each other
                int offsetDist = itObjSeq.getOffset() - eSeq.getOffset();
                if (offsetDist >= 0 ? offsetDist < eSeq.getSequenceLength() : -offsetDist < itObjSeq.getSequenceLength()) {
                    //calculate absolute overlap
                    int absoluteOverlap = Math.min(eSeq.getOffset() + eSeq.getSequenceLength(), itObjSeq.getOffset() + itObjSeq.getSequenceLength()) - Math.max(eSeq.getOffset(), itObjSeq.getOffset());
                    float relativeOverlap = absoluteOverlap / (float) Math.max(eSeq.getSequenceLength(), itObjSeq.getSequenceLength());
                    // Compare distances
                    if (relativeOverlap > allowedRelativeOverlap) {
                        if (e.getDistance() < itObj.getDistance()) {
                            toRemove.add(itObj);
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
        for (RankedAbstractObject rao : toRemove) {
            this.remove(rao);
        }
        return super.add(e);
    }
}
