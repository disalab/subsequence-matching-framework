# README #
The SMF library implements a generic framework for subsequence matching as proposed in paper: 

D. Novak, P. Volny, and P. Zezula, “Generic Subsequence Matching Framework: Modularity, Flexibility, Efficiency,” arXiv:1206.2510v1, Multimedia; Data Structures and Algorithms; Information Retrieval arXiv:1206.2510v1, 2012.

If you use the library for academic purposes, please reference these papers in your work.

### Who do I talk to? ###

* David Novak <david.novak(at)fi.muni.cz>

### Licence ###

SMF library is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. MESSIF library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.