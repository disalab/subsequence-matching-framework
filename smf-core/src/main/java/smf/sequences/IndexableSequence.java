package smf.sequences;

import messif.objects.LocalAbstractObject;

/**
 * @param <T> type of the sequence data, usually a static array of a primitive
 * type or {@link java.util.List}
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public interface IndexableSequence<T> extends Sequence<T> {

    /**
     * Returns this sequence.
     *
     * @return this sequence
     */
    public LocalAbstractObject getSequence();

    /**
     * Returns the sequence item at offset-th position.
     *
     * @param offset offset of the desired sequence item
     * @return the sequence item at offset-th position
     */
    public LocalAbstractObject getSequenceItem(int offset);
}
