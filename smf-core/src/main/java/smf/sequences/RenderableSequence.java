/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequences;

/**
 * Objects implementing this class provide access to their data in a form of sequence of numbers or
 *  several sequences of numbers.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public interface RenderableSequence {
    
    /**
     * Access object data in a form of sequence of numbers or several sequences of numbers.
     * @return object data in a form of sequence of numbers or several sequences of numbers
     */
    public float [] [] getDataForRendering();
    
}
