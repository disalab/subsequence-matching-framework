/**
 * Support for working with generic sequences. This package contains
 * interfaces for slicing sequences into subsequences and transforming
 * them (e.g. by a reduction) into different sequences.
 */
package smf.sequences;

