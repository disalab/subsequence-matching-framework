/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.sequences;

/**
 * Objects implementing this class provide generic access to their sequence data.
 * Sequences are usually static arrays of primitive types, however, this interface
 * can be used also with sets or other data types. The position of a particular
 * sequence value is references by non-negative integers starting from zero.
 *
 * @param <T> the type of the sequence data, usually a static array of a primitive type
 *          or {@link java.util.List}
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public interface Sequence<T> {
    
    /**
     * Parameter string for sequence offset 
     * that should be used within operations used by subsequence matching framework 
     */
    public static final String PARAM_SEQ_OFFSET = "seq_offset";
    
    /**
     * Parameter string for sequence length
     * that should be used within operations used by subsequence matching framework 
     */
    public static final String PARAM_SEQ_LENGTH = "seq_length";

    /**
     * Parameter string for minimum sequence length 
     */
    public static final String PARAM_MIN_SEQ_LENGTH = "min_seq_length";

    /**
     * Parameter string for maximum sequence length
     */
    public static final String PARAM_MAX_SEQ_LENGTH = "max_seq_length";

    /**
     * Parameter string for specification if the parent sequence link should be
     *  set or not.
     */
    public static final String PARAM_SET_PARENT_SEQ = "set_parent_seq";
    
    /**
     * Returns the data of this sequence.
     * Note that a copy of the stored data is returned.
     * @return the data of this sequence
     */
    public abstract T getSequenceData();

    /**
     * Returns the number of elements of this sequence.
     * @return the number of elements of this sequence
     */
    public abstract int getSequenceLength();

    /**
     * Returns the class of the data in this sequence.
     * This is usually a static array of a primitive type or {@link java.util.List}.
     * @return the class of elements of this sequence
     */
    public abstract Class<? extends T> getSequenceDataClass();

    /**
     * Returns a subsequence data from this sequence.
     * Note that a copy of the stored data is returned.
     * @param from the initial index of the subsequence element to be copied, inclusive
     * @param to the final index of the subsequence element to be copied, exclusive
     * @return a subsequence data from this sequence
     */
    public abstract T getSubsequenceData(int from, int to);

    /**
     * Returns the original {@link Sequence} from which this subsequence was fetched.
     * @return the original (parent) sequence
     */
    public abstract Sequence<? extends T> getOriginalSequence();

    /**
     * Returns the offset value representing the starting position of this subsequence
     * within the original {@link Sequence}.
     * @return the starting position of this subsequence
     */
    public abstract int getOffset();

    /**
     * Returns the locator string for the original sequence.
     * @return the locator of the original sequence
     */
    public abstract String getOriginalSequenceLocator();

    /**
     * Computes distance between a specified piece of this sequence and the given sequence.
     * It is supposed to be called from the implementation of a distance between two sequences.
     * @param thisPieceOffset The offset of an item in this sequence
     * @param other The second sequence
     * @param otherPieceOffset The offset of an item in the second sequence
     * @return the distance between a specified piece of this sequence and the given sequence
     */
    public abstract float getPiecewiseDist(int thisPieceOffset, Sequence<T> other, int otherPieceOffset);

}
