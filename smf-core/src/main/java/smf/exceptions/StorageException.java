/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.exceptions;

/**
 * Thrown when either the whole sequence storage or the distance index fails
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public class StorageException extends Exception {
    
    /** Serial ID for serialization */
    private static final long serialVersionUID = 19602L;

    /**
     * Constructs an instance of <code>StorageException</code> with the specified cause.
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method). (A <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     */
    public StorageException(Throwable cause) {
        super(cause);
    }
    
    /**
     * Constructs an instance of <code>StorageException</code> with the specified message.
     * @param message typically description of the cause
     */
    public StorageException(String message) {
        super(message);
    }

    /**
     * Default constructor
     */
    public StorageException() {
    }
    
}
