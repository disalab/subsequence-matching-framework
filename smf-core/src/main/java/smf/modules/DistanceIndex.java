/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.util.RankedAbstractObject;
import messif.operations.QueryOperation;
import messif.utility.reflection.NoSuchInstantiatorException;
import smf.exceptions.StorageException;
import smf.sequences.Sequence;
import smf.sequences.SequenceFactory;

/**
 * Module "distance index" that encapsulates an index built, typically, on 
 *   distances of the same length (or using DTW). It can return "k" most similar sequences.
 * 
 * @param <I> data+distance 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public abstract class DistanceIndex<I extends LocalAbstractObject> {
    
    /**
     * Stores given object in the distance index 
     * @param object object to be stored (data + distance)
     * @throws AlgorithmMethodException if the storage did not
     */
    public abstract void addObject(I object) throws StorageException;

    /**
     * Stores given objects in the distance index 
     * @param objects objects to be stored (data + distance)
     * @throws AlgorithmMethodException if the storage did not
     */
    public abstract void addObjects(Collection<I> objects) throws StorageException;
    
    /**
     * Given a sequence and slicer, this method creates a set of sequences by the slicer (and 
     *  {@link SequenceFactory}) and stores these parts into the index.
     * @param <T> basic sequence data type, e.g. float[]
     * @param s sequence to be sliced
     * @param slicer slicer to be applied
     * @return number of created and inserted slices
     * @throws StorageException if there was a problem during creation of subsequences or storing them
     */
    public <T> int sliceAndAdd(Sequence<T> s, SequenceSlicer slicer) throws StorageException {
        try {
            List<I> subList = SequenceFactory.create(s, slicer);
            addObjects(subList);
            return subList.size();
        } catch (NoSuchInstantiatorException ex) {
            throw new StorageException(ex);
        } catch (InvocationTargetException ex) {
            throw new StorageException(ex);
        }
    }
    
    /**
     * Given an object (e.g. a slice of sequence), find the {@code k} closest objects
     *  stored in this index.
     * @param object query object (sequence0
     * @param k number of required nearest neighbors
     * @return iterator over the answer (ranked objects)
     * @throws StorageException if the search fails
     */
    public abstract Iterator<RankedAbstractObject> findNearest(I object, int k) throws StorageException;
    
    /**
     * Fallback method to process any query operation (typically on encapsulated bucket)
     * @param operation query operation to be processed
     * @return number of objects added to the operation
     */
    public abstract int processQuery(QueryOperation<?> operation);

    /**
     * Returns the number of managed sequences
     * @return returns the number of managed sequences
     */
    public abstract int getObjectCount();
    
    @SuppressWarnings("FinalizeDeclaration")
    @Override
    public abstract void finalize() throws Throwable;

    public abstract void destroy() throws Throwable;
    
}
