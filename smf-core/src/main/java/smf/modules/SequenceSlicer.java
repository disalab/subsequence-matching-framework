/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules;

import smf.sequences.SequenceSlice;
import smf.sequences.Sequence;
import java.util.List;

/**
 * Provides a functionality of slicing a {@link Sequence} into several {@link SequenceSlice}s.
 * The particular implementation of this interface decides whether the slices
 * are disjoint, overlapping, etc.
 *
 * @param <T> the type of the sequence data, usually a static array of a primitive type
 *          or {@link java.util.List}
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public interface SequenceSlicer<T> {
    
    /**
     * Slices a given {@code sequence} into several {@link SequenceSlice}s.
     *
     * @param sequence the sequence to slice
     * @return a list of resulting {@link SequenceSlice slices}
     */
    public abstract List<? extends SequenceSlice<T>> slice(Sequence<? extends T> sequence);
}
