/*
 *  This file is part of Subsequence Mathing Framework (SMF) library
 *
 *  SMF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Skip Graphs library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Skip Graphs library.  If not, see <http://www.gnu.org/licenses/>.
 */
package smf.modules;

import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.QueryOperation;
import smf.exceptions.StorageException;
import smf.sequences.Sequence;

/**
 * Interface of module "Sequence storage" which provides unified (and efficient) 
 *  access to whole data sequences.
 * 
 * TODO: add grouped subsequence retrieval
 * 
 * @param <O> type of sequence
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Petr Volny, Masaryk University, Brno, Czech Republic, volny.petr@gmail.com
 */
public interface SequenceStorage<T, O extends Sequence<T>> {
    
    /**
     * Given a locator, this method returns a subsequence of the stored sequence with given locator or null.
     *  The returned subsequence has its parent sequence set {@link smf.sequences.Sequence#getOriginalSequence() }
     * @param locator identifier of object to be returned
     * @param from index of the start subsequences (inclusive); if lower than 0, it is automatically counted as 0
     * @param to end index of the subsequence (exclusive); if this index larger than sequence length,
     *   it is cut down to query length
     * 
     * @return the specified subsequence or null, if it is not stored
     * @throws StorageException if anything goes wrong
     */
    public O getSubsequence(String locator, int from, int to) throws StorageException;

    /**
     * Given a locator, this method returns a subsequence of the stored sequence with given locator or null.
     *  
     * @param locator identifier of object to be returned
     * @param from index of the start subsequences (inclusive); if lower than 0, it is automatically counted as 0
     * @param to end index of the subsequence (exclusive); if this index larger than sequence length,
     *   it is cut down to query length
     * @param storeParentSequence if true, the returned subsequence has its parent sequence 
     *   set {@link smf.sequences.Sequence#getOriginalSequence() }
     * 
     * @return the specified subsequence or null, if it is not stored
     * @throws StorageException if anything goes wrong
     */
    public O getSubsequence(String locator, int from, int to, boolean storeParentSequence) throws StorageException;
    
    /**
     * Given a locator, this method returns the stored sequence with given locator or null
     * @param locator identifier of object to be returned
     * @return the specified sequence or null, if it is not stored
     */
    public O getSequence(String locator);
    
    /**
     * Stores given sequence in the storage; it must be LocaAbstractObject at the same time
     * @param sequence sequence/object to be stored
     * @throws StorageException if anything goes wrong
     */
    public void insertSequence(O sequence) throws StorageException;

    /**
     * Fallback method to process any query operation (typically on encapsulated bucket)
     * @param operation query operation to be processed
     * @return number of objects added to the operation
     */
    public int processQuery(QueryOperation<?> operation);

    /**
     * Returns the number of managed sequences
     * @return returns the number of managed sequences
     */
    public int getObjectCount();
    
    /**
     * Returns iterator over all objects from this storage.
     * @return iterator over all objects from this storage
     */
    public abstract AbstractObjectIterator<LocalAbstractObject> getAllObjects();
    
    @SuppressWarnings("FinalizeDeclaration")
    public abstract void finalize() throws Throwable;

    public abstract void destroy() throws Throwable;
}
